//
// SbsProtoDTO.m
// Pods
//

#import "SbsProtoDTO.h"

@implementation SBSUserProtoDTO

@synthesize _id;
@synthesize username;
- (id)initWithDictionary:(NSDictionary *)dictionary {

    self = [super init];

    if (self) {
        self._id = [self deserializeFromJson:dictionary key:@"id" class:NSString.class repeated:NO optional:NO];
        self.username = [self deserializeFromJson:dictionary key:@"username" class:NSString.class repeated:NO optional:NO];
    }
    return self;
}
- (NSDictionary *)params {
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
[self serializeToJson:dictionary key:@"id" value:_id class:NSString.class repeated:NO optional:NO];
[self serializeToJson:dictionary key:@"username" value:username class:NSString.class repeated:NO optional:NO];
    return [NSDictionary dictionaryWithDictionary:dictionary];
}
@end

@implementation SBSUserBasicDTO

@synthesize _id;
@synthesize username;
- (id)initWithDictionary:(NSDictionary *)dictionary {

    self = [super init];

    if (self) {
        self._id = [self deserializeFromJson:dictionary key:@"id" class:NSString.class repeated:NO optional:NO];
        self.username = [self deserializeFromJson:dictionary key:@"username" class:NSString.class repeated:NO optional:NO];
    }
    return self;
}
- (NSDictionary *)params {
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
[self serializeToJson:dictionary key:@"id" value:_id class:NSString.class repeated:NO optional:NO];
[self serializeToJson:dictionary key:@"username" value:username class:NSString.class repeated:NO optional:NO];
    return [NSDictionary dictionaryWithDictionary:dictionary];
}
@end

@implementation SBSDeviceServiceDTO

@synthesize kind;
@synthesize name;
@synthesize image_url;
- (id)initWithDictionary:(NSDictionary *)dictionary {

    self = [super init];

    if (self) {
        self.kind = [self deserializeFromJson:dictionary key:@"kind" class:NSString.class repeated:NO optional:NO];
        self.name = [self deserializeFromJson:dictionary key:@"name" class:NSString.class repeated:NO optional:NO];
        self.image_url = [self deserializeFromJson:dictionary key:@"image_url" class:NSString.class repeated:NO optional:NO];
    }
    return self;
}
- (NSDictionary *)params {
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
[self serializeToJson:dictionary key:@"kind" value:kind class:NSString.class repeated:NO optional:NO];
[self serializeToJson:dictionary key:@"name" value:name class:NSString.class repeated:NO optional:NO];
[self serializeToJson:dictionary key:@"image_url" value:image_url class:NSString.class repeated:NO optional:NO];
    return [NSDictionary dictionaryWithDictionary:dictionary];
}
@end

@implementation SBSConnectedServiceDTO

@synthesize _id;
@synthesize kind;
@synthesize name;
@synthesize device_service;
- (id)initWithDictionary:(NSDictionary *)dictionary {

    self = [super init];

    if (self) {
        self._id = [self deserializeFromJson:dictionary key:@"id" class:NSString.class repeated:NO optional:NO];
        self.kind = [self deserializeFromJson:dictionary key:@"kind" class:NSString.class repeated:NO optional:NO];
        self.name = [self deserializeFromJson:dictionary key:@"name" class:NSString.class repeated:NO optional:NO];
        self.device_service = [self deserializeFromJson:dictionary key:@"device_service" class:SBSDeviceServiceDTO.class repeated:NO optional:NO];
    }
    return self;
}
- (NSDictionary *)params {
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
[self serializeToJson:dictionary key:@"id" value:_id class:NSString.class repeated:NO optional:NO];
[self serializeToJson:dictionary key:@"kind" value:kind class:NSString.class repeated:NO optional:NO];
[self serializeToJson:dictionary key:@"name" value:name class:NSString.class repeated:NO optional:NO];
[self serializeToJson:dictionary key:@"device_service" value:device_service class:SBSDeviceServiceDTO.class repeated:NO optional:NO];
    return [NSDictionary dictionaryWithDictionary:dictionary];
}
@end

@implementation SBSConversationProtoDTO

@synthesize _id;
- (id)initWithDictionary:(NSDictionary *)dictionary {

    self = [super init];

    if (self) {
        self._id = [self deserializeFromJson:dictionary key:@"id" class:NSString.class repeated:NO optional:NO];
    }
    return self;
}
- (NSDictionary *)params {
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
[self serializeToJson:dictionary key:@"id" value:_id class:NSString.class repeated:NO optional:NO];
    return [NSDictionary dictionaryWithDictionary:dictionary];
}
@end

@implementation SBSMessageProtoDTO

@synthesize _id;
@synthesize conversation_id;
@synthesize kind;
@synthesize user;
@synthesize post_mts;
@synthesize text;
@synthesize image_url;
- (id)initWithDictionary:(NSDictionary *)dictionary {

    self = [super init];

    if (self) {
        self._id = [self deserializeFromJson:dictionary key:@"id" class:NSString.class repeated:NO optional:NO];
        self.conversation_id = [self deserializeFromJson:dictionary key:@"conversation_id" class:NSString.class repeated:NO optional:NO];
        self.kind = [self deserializeFromJson:dictionary key:@"kind" class:NSNumber.class repeated:NO optional:NO];
        self.user = [self deserializeFromJson:dictionary key:@"user" class:SBSUserBasicDTO.class repeated:NO optional:NO];
        self.post_mts = [self deserializeFromJson:dictionary key:@"post_mts" class:NSNumber.class repeated:NO optional:NO];
        self.text = [self deserializeFromJson:dictionary key:@"text" class:NSString.class repeated:NO optional:YES];
        self.image_url = [self deserializeFromJson:dictionary key:@"image_url" class:NSString.class repeated:NO optional:YES];
    }
    return self;
}
- (NSDictionary *)params {
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
[self serializeToJson:dictionary key:@"id" value:_id class:NSString.class repeated:NO optional:NO];
[self serializeToJson:dictionary key:@"conversation_id" value:conversation_id class:NSString.class repeated:NO optional:NO];
[self serializeToJson:dictionary key:@"kind" value:kind class:NSNumber.class repeated:NO optional:NO];
[self serializeToJson:dictionary key:@"user" value:user class:SBSUserBasicDTO.class repeated:NO optional:NO];
[self serializeToJson:dictionary key:@"post_mts" value:post_mts class:NSNumber.class repeated:NO optional:NO];
[self serializeToJson:dictionary key:@"text" value:text class:NSString.class repeated:NO optional:YES];
[self serializeToJson:dictionary key:@"image_url" value:image_url class:NSString.class repeated:NO optional:YES];
    return [NSDictionary dictionaryWithDictionary:dictionary];
}
@end

@implementation SBSApiConversation_ListRequestDTO

- (id)initWithDictionary:(NSDictionary *)dictionary {

    self = [super init];

    if (self) {
    }
    return self;
}
- (NSDictionary *)params {
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    return [NSDictionary dictionaryWithDictionary:dictionary];
}
@end

@implementation SBSApiConversation_ListResponseDTO

@synthesize conversations;
- (id)initWithDictionary:(NSDictionary *)dictionary {

    self = [super init];

    if (self) {
        self.conversations = [self deserializeFromJson:dictionary key:@"conversations" class:SBSApiConversation_ListResponse_ConversationDataDTO.class repeated:YES optional:NO];
    }
    return self;
}
- (NSDictionary *)params {
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
[self serializeToJson:dictionary key:@"conversations" value:conversations class:SBSApiConversation_ListResponse_ConversationDataDTO.class repeated:YES optional:NO];
    return [NSDictionary dictionaryWithDictionary:dictionary];
}
@end

@implementation SBSApiConversation_GetRequestDTO

@synthesize conversation_id;
@synthesize start_mts;
@synthesize end_mts;
@synthesize limit;
- (id)initWithDictionary:(NSDictionary *)dictionary {

    self = [super init];

    if (self) {
        self.conversation_id = [self deserializeFromJson:dictionary key:@"conversation_id" class:NSString.class repeated:NO optional:YES];
        self.start_mts = [self deserializeFromJson:dictionary key:@"start_mts" class:NSNumber.class repeated:NO optional:YES];
        self.end_mts = [self deserializeFromJson:dictionary key:@"end_mts" class:NSNumber.class repeated:NO optional:YES];
        self.limit = [self deserializeFromJson:dictionary key:@"limit" class:NSNumber.class repeated:NO optional:YES];
    }
    return self;
}
- (NSDictionary *)params {
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
[self serializeToJson:dictionary key:@"conversation_id" value:conversation_id class:NSString.class repeated:NO optional:YES];
[self serializeToJson:dictionary key:@"start_mts" value:start_mts class:NSNumber.class repeated:NO optional:YES];
[self serializeToJson:dictionary key:@"end_mts" value:end_mts class:NSNumber.class repeated:NO optional:YES];
[self serializeToJson:dictionary key:@"limit" value:limit class:NSNumber.class repeated:NO optional:YES];
    return [NSDictionary dictionaryWithDictionary:dictionary];
}
@end

@implementation SBSApiConversation_GetResponseDTO

@synthesize conversation;
@synthesize messages;
- (id)initWithDictionary:(NSDictionary *)dictionary {

    self = [super init];

    if (self) {
        self.conversation = [self deserializeFromJson:dictionary key:@"conversation" class:SBSConversationProtoDTO.class repeated:NO optional:YES];
        self.messages = [self deserializeFromJson:dictionary key:@"messages" class:SBSMessageProtoDTO.class repeated:YES optional:NO];
    }
    return self;
}
- (NSDictionary *)params {
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
[self serializeToJson:dictionary key:@"conversation" value:conversation class:SBSConversationProtoDTO.class repeated:NO optional:YES];
[self serializeToJson:dictionary key:@"messages" value:messages class:SBSMessageProtoDTO.class repeated:YES optional:NO];
    return [NSDictionary dictionaryWithDictionary:dictionary];
}
@end

@implementation SBSApiConversation_SendMessageRequestDTO

@synthesize conversation_id;
@synthesize text;
@synthesize from_voice;
- (id)initWithDictionary:(NSDictionary *)dictionary {

    self = [super init];

    if (self) {
        self.conversation_id = [self deserializeFromJson:dictionary key:@"conversation_id" class:NSString.class repeated:NO optional:YES];
        self.text = [self deserializeFromJson:dictionary key:@"text" class:NSString.class repeated:NO optional:YES];
        self.from_voice = [self deserializeFromJson:dictionary key:@"from_voice" class:NSNumber.class repeated:NO optional:YES];
    }
    return self;
}
- (NSDictionary *)params {
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
[self serializeToJson:dictionary key:@"conversation_id" value:conversation_id class:NSString.class repeated:NO optional:YES];
[self serializeToJson:dictionary key:@"text" value:text class:NSString.class repeated:NO optional:YES];
[self serializeToJson:dictionary key:@"from_voice" value:from_voice class:NSNumber.class repeated:NO optional:YES];
    return [NSDictionary dictionaryWithDictionary:dictionary];
}
@end

@implementation SBSApiConversation_SendMessageResponseDTO

@synthesize message;
- (id)initWithDictionary:(NSDictionary *)dictionary {

    self = [super init];

    if (self) {
        self.message = [self deserializeFromJson:dictionary key:@"message" class:SBSMessageProtoDTO.class repeated:NO optional:YES];
    }
    return self;
}
- (NSDictionary *)params {
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
[self serializeToJson:dictionary key:@"message" value:message class:SBSMessageProtoDTO.class repeated:NO optional:YES];
    return [NSDictionary dictionaryWithDictionary:dictionary];
}
@end

@implementation SBSApiConversation_GetFeedbacksRequestDTO

@synthesize conversation_id;
- (id)initWithDictionary:(NSDictionary *)dictionary {

    self = [super init];

    if (self) {
        self.conversation_id = [self deserializeFromJson:dictionary key:@"conversation_id" class:NSString.class repeated:NO optional:YES];
    }
    return self;
}
- (NSDictionary *)params {
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
[self serializeToJson:dictionary key:@"conversation_id" value:conversation_id class:NSString.class repeated:NO optional:YES];
    return [NSDictionary dictionaryWithDictionary:dictionary];
}
@end

@implementation SBSApiConversation_GetFeedbacksResponseDTO

@synthesize feedbacks;
- (id)initWithDictionary:(NSDictionary *)dictionary {

    self = [super init];

    if (self) {
        self.feedbacks = [self deserializeFromJson:dictionary key:@"feedbacks" class:SBSApiConversation_GetFeedbacksResponse_FeedbackDTO.class repeated:YES optional:NO];
    }
    return self;
}
- (NSDictionary *)params {
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
[self serializeToJson:dictionary key:@"feedbacks" value:feedbacks class:SBSApiConversation_GetFeedbacksResponse_FeedbackDTO.class repeated:YES optional:NO];
    return [NSDictionary dictionaryWithDictionary:dictionary];
}
@end

@implementation SBSApiConversation_UpdateFeedbackRequestDTO

@synthesize feedback_id;
@synthesize intent_name;
@synthesize variables;
- (id)initWithDictionary:(NSDictionary *)dictionary {

    self = [super init];

    if (self) {
        self.feedback_id = [self deserializeFromJson:dictionary key:@"feedback_id" class:NSString.class repeated:NO optional:YES];
        self.intent_name = [self deserializeFromJson:dictionary key:@"intent_name" class:NSString.class repeated:NO optional:NO];
        self.variables = [self deserializeFromJson:dictionary key:@"variables" class:SBSApiConversation_ParsedMessageVarDTO.class repeated:YES optional:NO];
    }
    return self;
}
- (NSDictionary *)params {
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
[self serializeToJson:dictionary key:@"feedback_id" value:feedback_id class:NSString.class repeated:NO optional:YES];
[self serializeToJson:dictionary key:@"intent_name" value:intent_name class:NSString.class repeated:NO optional:NO];
[self serializeToJson:dictionary key:@"variables" value:variables class:SBSApiConversation_ParsedMessageVarDTO.class repeated:YES optional:NO];
    return [NSDictionary dictionaryWithDictionary:dictionary];
}
@end

@implementation SBSApiConversation_UpdateFeedbackResponseDTO

- (id)initWithDictionary:(NSDictionary *)dictionary {

    self = [super init];

    if (self) {
    }
    return self;
}
- (NSDictionary *)params {
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    return [NSDictionary dictionaryWithDictionary:dictionary];
}
@end

@implementation SBSApiConversation_ParsedMessageVarDTO

@synthesize name;
@synthesize is_found;
@synthesize value;
- (id)initWithDictionary:(NSDictionary *)dictionary {

    self = [super init];

    if (self) {
        self.name = [self deserializeFromJson:dictionary key:@"name" class:NSString.class repeated:NO optional:NO];
        self.is_found = [self deserializeFromJson:dictionary key:@"is_found" class:NSNumber.class repeated:NO optional:NO];
        self.value = [self deserializeFromJson:dictionary key:@"value" class:NSString.class repeated:NO optional:YES];
    }
    return self;
}
- (NSDictionary *)params {
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
[self serializeToJson:dictionary key:@"name" value:name class:NSString.class repeated:NO optional:NO];
[self serializeToJson:dictionary key:@"is_found" value:is_found class:NSNumber.class repeated:NO optional:NO];
[self serializeToJson:dictionary key:@"value" value:value class:NSString.class repeated:NO optional:YES];
    return [NSDictionary dictionaryWithDictionary:dictionary];
}
@end

@implementation SBSApiConversation_ReportRequestDTO

@synthesize description;
- (id)initWithDictionary:(NSDictionary *)dictionary {

    self = [super init];

    if (self) {
        self.description = [self deserializeFromJson:dictionary key:@"description" class:NSString.class repeated:NO optional:NO];
    }
    return self;
}
- (NSDictionary *)params {
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
[self serializeToJson:dictionary key:@"description" value:description class:NSString.class repeated:NO optional:NO];
    return [NSDictionary dictionaryWithDictionary:dictionary];
}
@end

@implementation SBSApiConversation_ReportResponseDTO

- (id)initWithDictionary:(NSDictionary *)dictionary {

    self = [super init];

    if (self) {
    }
    return self;
}
- (NSDictionary *)params {
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    return [NSDictionary dictionaryWithDictionary:dictionary];
}
@end

@implementation SBSApiDevice_ListServicesRequestDTO

- (id)initWithDictionary:(NSDictionary *)dictionary {

    self = [super init];

    if (self) {
    }
    return self;
}
- (NSDictionary *)params {
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    return [NSDictionary dictionaryWithDictionary:dictionary];
}
@end

@implementation SBSApiDevice_ListServicesResponseDTO

@synthesize connected_services;
@synthesize add_links;
- (id)initWithDictionary:(NSDictionary *)dictionary {

    self = [super init];

    if (self) {
        self.connected_services = [self deserializeFromJson:dictionary key:@"connected_services" class:SBSConnectedServiceDTO.class repeated:YES optional:NO];
        self.add_links = [self deserializeFromJson:dictionary key:@"add_links" class:SBSApiDevice_ListServicesResponse_AddLinkDTO.class repeated:YES optional:NO];
    }
    return self;
}
- (NSDictionary *)params {
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
[self serializeToJson:dictionary key:@"connected_services" value:connected_services class:SBSConnectedServiceDTO.class repeated:YES optional:NO];
[self serializeToJson:dictionary key:@"add_links" value:add_links class:SBSApiDevice_ListServicesResponse_AddLinkDTO.class repeated:YES optional:NO];
    return [NSDictionary dictionaryWithDictionary:dictionary];
}
@end

@implementation SBSApiDevice_DisconnectServiceRequestDTO

@synthesize _id;
- (id)initWithDictionary:(NSDictionary *)dictionary {

    self = [super init];

    if (self) {
        self._id = [self deserializeFromJson:dictionary key:@"id" class:NSString.class repeated:NO optional:NO];
    }
    return self;
}
- (NSDictionary *)params {
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
[self serializeToJson:dictionary key:@"id" value:_id class:NSString.class repeated:NO optional:NO];
    return [NSDictionary dictionaryWithDictionary:dictionary];
}
@end

@implementation SBSApiDevice_DisconnectServiceResponseDTO

@synthesize error;
- (id)initWithDictionary:(NSDictionary *)dictionary {

    self = [super init];

    if (self) {
        self.error = [self deserializeFromJson:dictionary key:@"error" class:NSString.class repeated:NO optional:YES];
    }
    return self;
}
- (NSDictionary *)params {
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
[self serializeToJson:dictionary key:@"error" value:error class:NSString.class repeated:NO optional:YES];
    return [NSDictionary dictionaryWithDictionary:dictionary];
}
@end

@implementation SBSApiUser_AuthenticateRequestDTO

@synthesize username;
@synthesize password;
@synthesize create_account;
@synthesize email;
- (id)initWithDictionary:(NSDictionary *)dictionary {

    self = [super init];

    if (self) {
        self.username = [self deserializeFromJson:dictionary key:@"username" class:NSString.class repeated:NO optional:NO];
        self.password = [self deserializeFromJson:dictionary key:@"password" class:NSString.class repeated:NO optional:YES];
        self.create_account = [self deserializeFromJson:dictionary key:@"create_account" class:NSNumber.class repeated:NO optional:YES];
        self.email = [self deserializeFromJson:dictionary key:@"email" class:NSString.class repeated:NO optional:YES];
    }
    return self;
}
- (NSDictionary *)params {
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
[self serializeToJson:dictionary key:@"username" value:username class:NSString.class repeated:NO optional:NO];
[self serializeToJson:dictionary key:@"password" value:password class:NSString.class repeated:NO optional:YES];
[self serializeToJson:dictionary key:@"create_account" value:create_account class:NSNumber.class repeated:NO optional:YES];
[self serializeToJson:dictionary key:@"email" value:email class:NSString.class repeated:NO optional:YES];
    return [NSDictionary dictionaryWithDictionary:dictionary];
}
@end

@implementation SBSApiUser_LoginRequestDTO

@synthesize username;
@synthesize password;
- (id)initWithDictionary:(NSDictionary *)dictionary {

    self = [super init];

    if (self) {
        self.username = [self deserializeFromJson:dictionary key:@"username" class:NSString.class repeated:NO optional:NO];
        self.password = [self deserializeFromJson:dictionary key:@"password" class:NSString.class repeated:NO optional:NO];
    }
    return self;
}
- (NSDictionary *)params {
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
[self serializeToJson:dictionary key:@"username" value:username class:NSString.class repeated:NO optional:NO];
[self serializeToJson:dictionary key:@"password" value:password class:NSString.class repeated:NO optional:NO];
    return [NSDictionary dictionaryWithDictionary:dictionary];
}
@end

@implementation SBSApiUser_RegisterRequestDTO

@synthesize username;
@synthesize password;
@synthesize email;
- (id)initWithDictionary:(NSDictionary *)dictionary {

    self = [super init];

    if (self) {
        self.username = [self deserializeFromJson:dictionary key:@"username" class:NSString.class repeated:NO optional:NO];
        self.password = [self deserializeFromJson:dictionary key:@"password" class:NSString.class repeated:NO optional:NO];
        self.email = [self deserializeFromJson:dictionary key:@"email" class:NSString.class repeated:NO optional:NO];
    }
    return self;
}
- (NSDictionary *)params {
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
[self serializeToJson:dictionary key:@"username" value:username class:NSString.class repeated:NO optional:NO];
[self serializeToJson:dictionary key:@"password" value:password class:NSString.class repeated:NO optional:NO];
[self serializeToJson:dictionary key:@"email" value:email class:NSString.class repeated:NO optional:NO];
    return [NSDictionary dictionaryWithDictionary:dictionary];
}
@end

@implementation SBSApiUser_AuthenticateResponseDTO

@synthesize user;
@synthesize auth_token;
@synthesize channel_name;
@synthesize error_message;
- (id)initWithDictionary:(NSDictionary *)dictionary {

    self = [super init];

    if (self) {
        self.user = [self deserializeFromJson:dictionary key:@"user" class:SBSUserProtoDTO.class repeated:NO optional:YES];
        self.auth_token = [self deserializeFromJson:dictionary key:@"auth_token" class:NSString.class repeated:NO optional:YES];
        self.channel_name = [self deserializeFromJson:dictionary key:@"channel_name" class:NSString.class repeated:NO optional:YES];
        self.error_message = [self deserializeFromJson:dictionary key:@"error_message" class:NSString.class repeated:NO optional:YES];
    }
    return self;
}
- (NSDictionary *)params {
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
[self serializeToJson:dictionary key:@"user" value:user class:SBSUserProtoDTO.class repeated:NO optional:YES];
[self serializeToJson:dictionary key:@"auth_token" value:auth_token class:NSString.class repeated:NO optional:YES];
[self serializeToJson:dictionary key:@"channel_name" value:channel_name class:NSString.class repeated:NO optional:YES];
[self serializeToJson:dictionary key:@"error_message" value:error_message class:NSString.class repeated:NO optional:YES];
    return [NSDictionary dictionaryWithDictionary:dictionary];
}
@end

@implementation SBSApiUser_MeRequestDTO

- (id)initWithDictionary:(NSDictionary *)dictionary {

    self = [super init];

    if (self) {
    }
    return self;
}
- (NSDictionary *)params {
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    return [NSDictionary dictionaryWithDictionary:dictionary];
}
@end

@implementation SBSApiUser_MeResponseDTO

@synthesize user;
@synthesize connected_services;
- (id)initWithDictionary:(NSDictionary *)dictionary {

    self = [super init];

    if (self) {
        self.user = [self deserializeFromJson:dictionary key:@"user" class:SBSUserProtoDTO.class repeated:NO optional:YES];
        self.connected_services = [self deserializeFromJson:dictionary key:@"connected_services" class:SBSConnectedServiceDTO.class repeated:YES optional:NO];
    }
    return self;
}
- (NSDictionary *)params {
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
[self serializeToJson:dictionary key:@"user" value:user class:SBSUserProtoDTO.class repeated:NO optional:YES];
[self serializeToJson:dictionary key:@"connected_services" value:connected_services class:SBSConnectedServiceDTO.class repeated:YES optional:NO];
    return [NSDictionary dictionaryWithDictionary:dictionary];
}
@end

@implementation SBSApiConversation_ListResponse_ConversationDataDTO

@synthesize conversation;
@synthesize last_message;
- (id)initWithDictionary:(NSDictionary *)dictionary {

    self = [super init];

    if (self) {
        self.conversation = [self deserializeFromJson:dictionary key:@"conversation" class:SBSConversationProtoDTO.class repeated:NO optional:NO];
        self.last_message = [self deserializeFromJson:dictionary key:@"last_message" class:SBSMessageProtoDTO.class repeated:NO optional:YES];
    }
    return self;
}
- (NSDictionary *)params {
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
[self serializeToJson:dictionary key:@"conversation" value:conversation class:SBSConversationProtoDTO.class repeated:NO optional:NO];
[self serializeToJson:dictionary key:@"last_message" value:last_message class:SBSMessageProtoDTO.class repeated:NO optional:YES];
    return [NSDictionary dictionaryWithDictionary:dictionary];
}
@end

@implementation SBSApiConversation_GetFeedbacksResponse_FeedbackDTO

@synthesize feedback_id;
@synthesize message_id;
@synthesize text;
@synthesize parses;
@synthesize accepted_parse;
- (id)initWithDictionary:(NSDictionary *)dictionary {

    self = [super init];

    if (self) {
        self.feedback_id = [self deserializeFromJson:dictionary key:@"feedback_id" class:NSString.class repeated:NO optional:NO];
        self.message_id = [self deserializeFromJson:dictionary key:@"message_id" class:NSString.class repeated:NO optional:NO];
        self.text = [self deserializeFromJson:dictionary key:@"text" class:NSString.class repeated:NO optional:NO];
        self.parses = [self deserializeFromJson:dictionary key:@"parses" class:SBSApiConversation_GetFeedbacksResponse_Feedback_ParsedMessageDTO.class repeated:YES optional:NO];
        self.accepted_parse = [self deserializeFromJson:dictionary key:@"accepted_parse" class:SBSApiConversation_GetFeedbacksResponse_Feedback_ParsedMessageDTO.class repeated:NO optional:YES];
    }
    return self;
}
- (NSDictionary *)params {
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
[self serializeToJson:dictionary key:@"feedback_id" value:feedback_id class:NSString.class repeated:NO optional:NO];
[self serializeToJson:dictionary key:@"message_id" value:message_id class:NSString.class repeated:NO optional:NO];
[self serializeToJson:dictionary key:@"text" value:text class:NSString.class repeated:NO optional:NO];
[self serializeToJson:dictionary key:@"parses" value:parses class:SBSApiConversation_GetFeedbacksResponse_Feedback_ParsedMessageDTO.class repeated:YES optional:NO];
[self serializeToJson:dictionary key:@"accepted_parse" value:accepted_parse class:SBSApiConversation_GetFeedbacksResponse_Feedback_ParsedMessageDTO.class repeated:NO optional:YES];
    return [NSDictionary dictionaryWithDictionary:dictionary];
}
@end

@implementation SBSApiDevice_ListServicesResponse_AddLinkDTO

@synthesize url;
@synthesize text;
@synthesize kind;
@synthesize device_service;
- (id)initWithDictionary:(NSDictionary *)dictionary {

    self = [super init];

    if (self) {
        self.url = [self deserializeFromJson:dictionary key:@"url" class:NSString.class repeated:NO optional:NO];
        self.text = [self deserializeFromJson:dictionary key:@"text" class:NSString.class repeated:NO optional:NO];
        self.kind = [self deserializeFromJson:dictionary key:@"kind" class:NSString.class repeated:NO optional:NO];
        self.device_service = [self deserializeFromJson:dictionary key:@"device_service" class:SBSDeviceServiceDTO.class repeated:NO optional:NO];
    }
    return self;
}
- (NSDictionary *)params {
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
[self serializeToJson:dictionary key:@"url" value:url class:NSString.class repeated:NO optional:NO];
[self serializeToJson:dictionary key:@"text" value:text class:NSString.class repeated:NO optional:NO];
[self serializeToJson:dictionary key:@"kind" value:kind class:NSString.class repeated:NO optional:NO];
[self serializeToJson:dictionary key:@"device_service" value:device_service class:SBSDeviceServiceDTO.class repeated:NO optional:NO];
    return [NSDictionary dictionaryWithDictionary:dictionary];
}
@end

@implementation SBSApiConversation_GetFeedbacksResponse_Feedback_ParsedMessageDTO

@synthesize intent_name;
@synthesize confidence;
@synthesize variables;
- (id)initWithDictionary:(NSDictionary *)dictionary {

    self = [super init];

    if (self) {
        self.intent_name = [self deserializeFromJson:dictionary key:@"intent_name" class:NSString.class repeated:NO optional:NO];
        self.confidence = [self deserializeFromJson:dictionary key:@"confidence" class:NSNumber.class repeated:NO optional:NO];
        self.variables = [self deserializeFromJson:dictionary key:@"variables" class:SBSApiConversation_ParsedMessageVarDTO.class repeated:YES optional:NO];
    }
    return self;
}
- (NSDictionary *)params {
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
[self serializeToJson:dictionary key:@"intent_name" value:intent_name class:NSString.class repeated:NO optional:NO];
[self serializeToJson:dictionary key:@"confidence" value:confidence class:NSNumber.class repeated:NO optional:NO];
[self serializeToJson:dictionary key:@"variables" value:variables class:SBSApiConversation_ParsedMessageVarDTO.class repeated:YES optional:NO];
    return [NSDictionary dictionaryWithDictionary:dictionary];
}
@end
