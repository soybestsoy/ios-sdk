//
//  SbsRequest.h
//  Pods
//
//  Created by Peter Hsu on 1/29/16.
//
//

#import <Foundation/Foundation.h>

@interface SbsRequest : NSObject

- (id)initWithEndpoint:(NSString *)endpoint
            parameters:(NSDictionary *)parameters
            httpMethod:(NSString *)httpMethod
         responseClass:(Class)responseClass;

@property (nonatomic, strong) NSDictionary *parameters;
@property (nonatomic, strong) NSString *endpoint;
@property (nonatomic, strong) NSString *httpMethod;
@property (nonatomic) Class responseClass;

@end
