//
//  BaseDTO.m
//  Pods
//
//  Created by Peter Hsu on 1/29/16.
//
//

#import "BaseDTO.h"

@implementation BaseDTO

- (id)initWithDictionary:(id)dictionary {
    self = [super init];
    if (self) {
        
    }
    return self;
}
- (NSDictionary *)params {
    return @{};
}

- (id)deserializeFromJson:(id)value class:(Class)clazz repeated:(BOOL)repeated optional:(BOOL)optional {
    if (repeated) {
        NSArray *array = (NSArray*) value;
        NSMutableArray *typedArray = [NSMutableArray arrayWithCapacity:[array count]];
        for (id item in array) {
            [typedArray addObject:[self deserializeFromJson:item class:clazz repeated:NO optional:NO]];
        }
        return [NSArray arrayWithArray:typedArray];
    } else {
        if ([clazz isSubclassOfClass:BaseDTO.class] && [value isKindOfClass:NSDictionary.class]) {
            return [[clazz alloc] initWithDictionary:value];
        } else {
            return value;
        }
    }
}
- (id)deserializeFromJson:(NSDictionary*)json key:(NSString*)key class:(Class)clazz repeated:(BOOL)repeated optional:(BOOL)optional {
    
    id object = [json objectForKey:key];
    return [self deserializeFromJson:object class:clazz repeated:repeated optional:optional];
}
- (id)serializeToJson:(id)value class:(Class)clazz repeated:(BOOL)repeated optional:(BOOL)optional {
    if (repeated) {
        NSArray *array = (NSArray*) value;
        NSMutableArray *typedArray = [NSMutableArray arrayWithCapacity:[array count]];
        for (id item in array) {
            [typedArray addObject:[self serializeToJson:item class:clazz repeated:NO optional:NO]];
        }
        return [NSArray arrayWithArray:typedArray];
    } else if ([clazz isSubclassOfClass:BaseDTO.class]) {
        return [value params];
    } else {
        return value;
    }
}
- (void)serializeToJson:(NSDictionary*)json key:(NSString*)key value:(id)value class:(Class)clazz repeated:(BOOL)repeated optional:(BOOL)optional  {
    
    if (value == nil && !optional) {
        // TODO: throw an error?
    }
    
    if (value != nil) {
        id typedValue = [self serializeToJson:value class:clazz repeated:repeated optional:optional];
        [json setValue:typedValue forKey:key];
    }
}
@end
