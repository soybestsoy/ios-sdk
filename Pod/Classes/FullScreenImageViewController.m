//
//  FullScreenImageViewController.m
//  Pods
//
//  Created by Peter Hsu on 2/15/16.
//
//

#import "FullScreenImageViewController.h"

@interface FullScreenImageViewController ()

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UIImage *image;

@property (nonatomic, strong) UIButton *closeButton;

@end

@implementation FullScreenImageViewController

- (id)initWithImage:(UIImage *)image {
    self = [super init];
    
    if (self) {
        _image = image;
    }
    
    return self;
}
#pragma mark -
#pragma mark - View Methods
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    // Creates a view Dictionary to be used in constraints
    NSDictionary *viewsDictionary;
    
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [self.view addSubview:self.scrollView];
    
    self.scrollView.backgroundColor = [UIColor blackColor];
    
    // Creates an image view with a test image
    self.imageView = [[UIImageView alloc] init];
    self.imageView.image = _image;
    
    // Add the imageview to the scrollview
    [self.scrollView addSubview:self.imageView];
    
    // Sets the following flag so that auto layout is used correctly
    self.scrollView.translatesAutoresizingMaskIntoConstraints = NO;
    self.imageView.translatesAutoresizingMaskIntoConstraints = NO;
    
    // Sets the scrollview delegate as self
    self.scrollView.delegate = self;
    
    // Creates references to the views
    UIScrollView *scrollView = self.scrollView;
    
    // Sets the image frame as the image size
    self.imageView.frame = CGRectMake(0, 0, _image.size.width, _image.size.height);
    
    // Tell the scroll view the size of the contents
    self.scrollView.contentSize = _image.size;
    
    // Set the constraints for the scroll view
    viewsDictionary = NSDictionaryOfVariableBindings(scrollView);
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[scrollView]|" options:0 metrics:0 views:viewsDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[scrollView]-(0)-|" options:0 metrics: 0 views:viewsDictionary]];
    
    // Add doubleTap recognizer to the scrollView
    UITapGestureRecognizer *doubleTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(scrollViewDoubleTapped:)];
    doubleTapRecognizer.numberOfTapsRequired = 2;
    doubleTapRecognizer.numberOfTouchesRequired = 1;
    [self.scrollView addGestureRecognizer:doubleTapRecognizer];
    
    // Add two finger recognizer to the scrollView
    UITapGestureRecognizer *twoFingerTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(scrollViewTwoFingerTapped:)];
    twoFingerTapRecognizer.numberOfTapsRequired = 1;
    twoFingerTapRecognizer.numberOfTouchesRequired = 2;
    [self.scrollView addGestureRecognizer:twoFingerTapRecognizer];
    
    _closeButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 35, 35)];
    [_closeButton setTitle:@"x" forState:UIControlStateNormal];
    [_closeButton addTarget:self
                     action:@selector(didSelectClose:)
           forControlEvents:UIControlEventTouchUpInside];
    [_closeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_closeButton setBackgroundColor:[UIColor clearColor]];
    
    [self.view addSubview:_closeButton];
}

- (void)didSelectClose:(id)sender {
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 
                             }];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // Setup the scrollview scales on viewWillAppear
    [self setupScales];
}

#pragma mark -
#pragma mark - Scroll View scales setup and center

#pragma mark -
#pragma Scroll handling

- (void)zoomAndCenterOnRect:(CGRect)rect animated:(BOOL)animated {
    [self.scrollView zoomToRect:rect animated:animated];
}

- (void)updateScales {
    if (self.scrollView.contentSize.width == 0 || self.scrollView.contentSize.height == 0) {
        return;
    }
    
    CGRect scrollViewFrame = self.scrollView.frame;
    CGFloat scaleWidth = scrollViewFrame.size.width / self.image.size.width;
    CGFloat scaleHeight = scrollViewFrame.size.height / self.image.size.height;
    CGFloat minScale = MIN(scaleWidth, scaleHeight);
    
    self.scrollView.minimumZoomScale = minScale;
    self.scrollView.maximumZoomScale = 6.0f;
    
    if (self.scrollView.zoomScale < minScale) {
        self.scrollView.zoomScale = minScale;
    }
    
}

-(void)setupScales {
    // Set up the minimum & maximum zoom scales
    
    self.scrollView.contentSize = self.imageView.image.size;
    
    if (self.image.size.width == 0 || self.image.size.height == 0) {
        return;
    }
    
    CGRect scrollViewFrame = self.scrollView.frame;
    CGFloat scaleWidth = scrollViewFrame.size.width / self.image.size.width;
    CGFloat scaleHeight = scrollViewFrame.size.height / self.image.size.height;
    CGFloat minScale = MIN(scaleWidth, scaleHeight);
    
    self.scrollView.minimumZoomScale = minScale;
    self.scrollView.maximumZoomScale = 6.0f;
    self.scrollView.zoomScale = minScale;
    
    [self centerScrollViewContents];
}

- (void)centerScrollViewContentsAnimated {
    [UIView animateWithDuration:0.2f
                     animations:^{
                         [self updateScales];
                         [self centerScrollViewContents];
                     }];
}

- (void)centerScrollViewContents {
    
    self.scrollView.alpha = 1;
    
    // This method centers the scroll view contents also used on did zoom
    CGSize boundsSize = self.scrollView.bounds.size;
    CGRect contentsFrame = self.imageView.frame;
    
    CGFloat xInset = 0;
    CGFloat yInset = 0;
    
    if (contentsFrame.size.width < boundsSize.width) {
        xInset = (boundsSize.width - contentsFrame.size.width) / 2.0f;
    }
    
    if (contentsFrame.size.height < boundsSize.height) {
        yInset = (boundsSize.height - contentsFrame.size.height) / 2.0f;
    }
    
    self.scrollView.contentInset = UIEdgeInsetsMake(yInset, xInset, yInset, xInset);
    
}


#pragma mark -
#pragma mark - ScrollView Delegate methods
- (UIView*)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    // Return the view that we want to zoom
    return self.imageView;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
    // The scroll view has zoomed, so we need to re-center the contents
    [self centerScrollViewContents];
}

#pragma mark -
#pragma mark - ScrollView gesture methods
- (void)scrollViewDoubleTapped:(UITapGestureRecognizer*)recognizer {
    // Get the location within the image view where we tapped
    CGPoint pointInView = [recognizer locationInView:self.imageView];
    
    // Get a zoom scale that's zoomed in slightly, capped at the maximum zoom scale specified by the scroll view
    CGFloat newZoomScale = self.scrollView.zoomScale * 1.5f;
    newZoomScale = MIN(newZoomScale, self.scrollView.maximumZoomScale);
    
    // Figure out the rect we want to zoom to, then zoom to it
    CGSize scrollViewSize = self.scrollView.bounds.size;
    
    CGFloat w = scrollViewSize.width / newZoomScale;
    CGFloat h = scrollViewSize.height / newZoomScale;
    CGFloat x = pointInView.x - (w / 2.0f);
    CGFloat y = pointInView.y - (h / 2.0f);
    
    CGRect rectToZoomTo = CGRectMake(x, y, w, h);
    
    [self.scrollView zoomToRect:rectToZoomTo animated:YES];
}

- (void)scrollViewTwoFingerTapped:(UITapGestureRecognizer*)recognizer {
    // Zoom out slightly, capping at the minimum zoom scale specified by the scroll view
    CGFloat newZoomScale = self.scrollView.zoomScale / 1.5f;
    newZoomScale = MAX(newZoomScale, self.scrollView.minimumZoomScale);
    [self.scrollView setZoomScale:newZoomScale animated:YES];
}

#pragma mark -
#pragma mark - Rotation

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    // When the orientation is changed the contentSize is reset when the frame changes. Setting this back to the relevant image size
    self.scrollView.contentSize = self.imageView.image.size;
    // Reset the scales depending on the change of values
    [self setupScales];
}

@end
