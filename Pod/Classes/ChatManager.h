//
//  ChatManager.h
//  Pods
//
//  Created by Peter Hsu on 2/2/16.
//
//

#import <Foundation/Foundation.h>
#import <Pusher/Pusher.h>
#import "SbsProtoDTO.h"

@protocol ChatDelegate <NSObject>

- (void)didReceiveMessage:(SBSMessageProtoDTO *)message;

@end


@interface ChatManager : NSObject <PTPusherDelegate>

+ (ChatManager *)sharedManager;

- (void)subscribeToChannel:(NSString *)channelName;
- (void)unsubscribe;

- (void)sendMessage:(NSString *)message completion:(void (^)(SBSMessageProtoDTO *message, NSError *error))completion;

- (void)getMessages:(NSString *)conversationId
     startTimestamp:(double)startTimestamp
       endTimestamp:(double)endTimestamp
         completion:(void (^)(NSArray *messages, BOOL endOfMessages, NSError *error))completion;

@property (nonatomic, unsafe_unretained) id <ChatDelegate> delegate;

@end
