//
//  SessionManager.h
//  Pods
//
//  Created by Peter Hsu on 2/2/16.
//
//

#import <Foundation/Foundation.h>

#import "SbsSession.h"
#import "SbsApiClient.h"


@protocol SessionManagerDelegate <NSObject>

- (void)onSessionStarted;
- (void)onSessionFailed;

@end

@interface SessionManager : NSObject

+ (SessionManager *)sharedManager;

- (BOOL)isAuthenticated;

- (void)authenticate:(NSString *)username
            password:(NSString *)password
         create_user:(BOOL)create_user
          completion:(void (^)(SBSApiUser_AuthenticateResponseDTO *response, NSError *error))completion;

@property (nonatomic, strong) SbsSession *currentSession;
@property (nonatomic, strong) SbsApiClient *apiClient;
@property (nonatomic, strong) NSString *apiKey;

@property (nonatomic, unsafe_unretained) id <SessionManagerDelegate>delegate;


@end
