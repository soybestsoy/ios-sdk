//
//  SoyChatTableViewCell.h
//  Pods
//
//  Created by Peter Hsu on 2/4/16.
//
//

#import <UIKit/UIKit.h>

@interface SoyChatTableViewCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *messageLabel;
@property (nonatomic, strong) IBOutlet UILabel *timestampLabel;
@property (nonatomic, strong) IBOutlet UIImageView *imageView;

@end
