//
//  FullScreenImageViewController.h
//  Pods
//
//  Created by Peter Hsu on 2/15/16.
//
//

#import <UIKit/UIKit.h>

@interface FullScreenImageViewController : UIViewController <UIScrollViewDelegate>

- (id)initWithImage:(UIImage *)image;

@end
