//
//  SbsSession.m
//  Pods
//
//  Created by Peter Hsu on 2/2/16.
//
//

#import "SbsSession.h"

@implementation SbsSession

@synthesize user;
@synthesize authToken;
@synthesize channelName;
@synthesize apiKey;

@end
