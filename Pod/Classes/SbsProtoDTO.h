//
// SbsProtoDTO.h
//

#import "BaseDTO.h"
@class SBSUserProtoDTO;
@class SBSUserBasicDTO;
@class SBSDeviceServiceDTO;
@class SBSConnectedServiceDTO;
@class SBSConversationProtoDTO;
@class SBSMessageProtoDTO;
@class SBSApiConversation_ListRequestDTO;
@class SBSApiConversation_ListResponseDTO;
@class SBSApiConversation_GetRequestDTO;
@class SBSApiConversation_GetResponseDTO;
@class SBSApiConversation_SendMessageRequestDTO;
@class SBSApiConversation_SendMessageResponseDTO;
@class SBSApiConversation_GetFeedbacksRequestDTO;
@class SBSApiConversation_GetFeedbacksResponseDTO;
@class SBSApiConversation_UpdateFeedbackRequestDTO;
@class SBSApiConversation_UpdateFeedbackResponseDTO;
@class SBSApiConversation_ParsedMessageVarDTO;
@class SBSApiConversation_ReportRequestDTO;
@class SBSApiConversation_ReportResponseDTO;
@class SBSApiDevice_ListServicesRequestDTO;
@class SBSApiDevice_ListServicesResponseDTO;
@class SBSApiDevice_DisconnectServiceRequestDTO;
@class SBSApiDevice_DisconnectServiceResponseDTO;
@class SBSApiUser_AuthenticateRequestDTO;
@class SBSApiUser_LoginRequestDTO;
@class SBSApiUser_RegisterRequestDTO;
@class SBSApiUser_AuthenticateResponseDTO;
@class SBSApiUser_MeRequestDTO;
@class SBSApiUser_MeResponseDTO;
@class SBSApiConversation_ListResponse_ConversationDataDTO;
@class SBSApiConversation_GetFeedbacksResponse_FeedbackDTO;
@class SBSApiDevice_ListServicesResponse_AddLinkDTO;
@class SBSApiConversation_GetFeedbacksResponse_Feedback_ParsedMessageDTO;

typedef enum {
    MediaKindText = 1,
    MediaKindImage = 2,
} MediaKind;

@interface SBSUserProtoDTO : BaseDTO
// {"name"=>"id", "number"=>1, "label"=>2, "type"=>9}
@property (nonatomic, strong) NSString *_id;
// {"name"=>"username", "number"=>2, "label"=>2, "type"=>9}
@property (nonatomic, strong) NSString *username;
@end

@interface SBSUserBasicDTO : BaseDTO
// {"name"=>"id", "number"=>1, "label"=>2, "type"=>9}
@property (nonatomic, strong) NSString *_id;
// {"name"=>"username", "number"=>2, "label"=>2, "type"=>9}
@property (nonatomic, strong) NSString *username;
@end

@interface SBSDeviceServiceDTO : BaseDTO
// {"name"=>"kind", "number"=>1, "label"=>2, "type"=>9}
@property (nonatomic, strong) NSString *kind;
// {"name"=>"name", "number"=>2, "label"=>2, "type"=>9}
@property (nonatomic, strong) NSString *name;
// {"name"=>"image_url", "number"=>3, "label"=>2, "type"=>9}
@property (nonatomic, strong) NSString *image_url;
@end

@interface SBSConnectedServiceDTO : BaseDTO
// {"name"=>"id", "number"=>1, "label"=>2, "type"=>9}
@property (nonatomic, strong) NSString *_id;
// {"name"=>"kind", "number"=>2, "label"=>2, "type"=>9}
@property (nonatomic, strong) NSString *kind;
// {"name"=>"name", "number"=>3, "label"=>2, "type"=>9}
@property (nonatomic, strong) NSString *name;
// {"name"=>"device_service", "number"=>4, "label"=>2, "type"=>11, "type_name"=>"SbsPb.DeviceService"}
@property (nonatomic, strong) SBSDeviceServiceDTO *device_service;
@end

@interface SBSConversationProtoDTO : BaseDTO
// {"name"=>"id", "number"=>1, "label"=>2, "type"=>9}
@property (nonatomic, strong) NSString *_id;
@end

@interface SBSMessageProtoDTO : BaseDTO
// {"name"=>"id", "number"=>1, "label"=>2, "type"=>9}
@property (nonatomic, strong) NSString *_id;
// {"name"=>"conversation_id", "number"=>2, "label"=>2, "type"=>9}
@property (nonatomic, strong) NSString *conversation_id;
// {"name"=>"kind", "number"=>6, "label"=>2, "type"=>14, "type_name"=>".SbsPb.MessageProto.Kind"}
@property (nonatomic, strong) NSNumber *kind;
// {"name"=>"user", "number"=>3, "label"=>2, "type"=>11, "type_name"=>"SbsPb.UserBasic"}
@property (nonatomic, strong) SBSUserBasicDTO *user;
// {"name"=>"post_mts", "number"=>4, "label"=>2, "type"=>4}
@property (nonatomic, strong) NSNumber *post_mts;
// {"name"=>"text", "number"=>5, "label"=>1, "type"=>9}
@property (nonatomic, strong) NSString *text;
// {"name"=>"image_url", "number"=>7, "label"=>1, "type"=>9}
@property (nonatomic, strong) NSString *image_url;
@end

@interface SBSApiConversation_ListRequestDTO : BaseDTO
@end

@interface SBSApiConversation_ListResponseDTO : BaseDTO
// {"name"=>"conversations", "number"=>1, "label"=>3, "type"=>11, "type_name"=>"SbsApi.Conversation.ListResponse.ConversationData"}
@property (nonatomic, strong) NSArray<SBSApiConversation_ListResponse_ConversationDataDTO*> *conversations;
@end

@interface SBSApiConversation_GetRequestDTO : BaseDTO
// {"name"=>"conversation_id", "number"=>1, "label"=>1, "type"=>9}
@property (nonatomic, strong) NSString *conversation_id;
// {"name"=>"start_mts", "number"=>2, "label"=>1, "type"=>4}
@property (nonatomic, strong) NSNumber *start_mts;
// {"name"=>"end_mts", "number"=>3, "label"=>1, "type"=>4}
@property (nonatomic, strong) NSNumber *end_mts;
// {"name"=>"limit", "number"=>4, "label"=>1, "type"=>4}
@property (nonatomic, strong) NSNumber *limit;
@end

@interface SBSApiConversation_GetResponseDTO : BaseDTO
// {"name"=>"conversation", "number"=>1, "label"=>1, "type"=>11, "type_name"=>"SbsPb.ConversationProto"}
@property (nonatomic, strong) SBSConversationProtoDTO *conversation;
// {"name"=>"messages", "number"=>2, "label"=>3, "type"=>11, "type_name"=>"SbsPb.MessageProto"}
@property (nonatomic, strong) NSArray<SBSMessageProtoDTO*> *messages;
@end

@interface SBSApiConversation_SendMessageRequestDTO : BaseDTO
// {"name"=>"conversation_id", "number"=>1, "label"=>1, "type"=>9}
@property (nonatomic, strong) NSString *conversation_id;
// {"name"=>"text", "number"=>2, "label"=>1, "type"=>9}
@property (nonatomic, strong) NSString *text;
// {"name"=>"from_voice", "number"=>3, "label"=>1, "type"=>8}
@property (nonatomic, strong) NSNumber *from_voice;
@end

@interface SBSApiConversation_SendMessageResponseDTO : BaseDTO
// {"name"=>"message", "number"=>1, "label"=>1, "type"=>11, "type_name"=>"SbsPb.MessageProto"}
@property (nonatomic, strong) SBSMessageProtoDTO *message;
@end

@interface SBSApiConversation_GetFeedbacksRequestDTO : BaseDTO
// {"name"=>"conversation_id", "number"=>1, "label"=>1, "type"=>9}
@property (nonatomic, strong) NSString *conversation_id;
@end

@interface SBSApiConversation_GetFeedbacksResponseDTO : BaseDTO
// {"name"=>"feedbacks", "number"=>1, "label"=>3, "type"=>11, "type_name"=>"SbsApi.Conversation.GetFeedbacksResponse.Feedback"}
@property (nonatomic, strong) NSArray<SBSApiConversation_GetFeedbacksResponse_FeedbackDTO*> *feedbacks;
@end

@interface SBSApiConversation_UpdateFeedbackRequestDTO : BaseDTO
// {"name"=>"feedback_id", "number"=>1, "label"=>1, "type"=>9}
@property (nonatomic, strong) NSString *feedback_id;
// {"name"=>"intent_name", "number"=>2, "label"=>2, "type"=>9}
@property (nonatomic, strong) NSString *intent_name;
// {"name"=>"variables", "number"=>3, "label"=>3, "type"=>11, "type_name"=>"SbsApi.Conversation.ParsedMessageVar"}
@property (nonatomic, strong) NSArray<SBSApiConversation_ParsedMessageVarDTO*> *variables;
@end

@interface SBSApiConversation_UpdateFeedbackResponseDTO : BaseDTO
@end

@interface SBSApiConversation_ParsedMessageVarDTO : BaseDTO
// {"name"=>"name", "number"=>1, "label"=>2, "type"=>9}
@property (nonatomic, strong) NSString *name;
// {"name"=>"is_found", "number"=>2, "label"=>2, "type"=>8}
@property (nonatomic, strong) NSNumber *is_found;
// {"name"=>"value", "number"=>3, "label"=>1, "type"=>9}
@property (nonatomic, strong) NSString *value;
@end

@interface SBSApiConversation_ReportRequestDTO : BaseDTO
// {"name"=>"description", "number"=>1, "label"=>2, "type"=>9}
@property (nonatomic, strong) NSString *description;
@end

@interface SBSApiConversation_ReportResponseDTO : BaseDTO
@end

@interface SBSApiDevice_ListServicesRequestDTO : BaseDTO
@end

@interface SBSApiDevice_ListServicesResponseDTO : BaseDTO
// {"name"=>"connected_services", "number"=>1, "label"=>3, "type"=>11, "type_name"=>"SbsPb.ConnectedService"}
@property (nonatomic, strong) NSArray<SBSConnectedServiceDTO*> *connected_services;
// {"name"=>"add_links", "number"=>2, "label"=>3, "type"=>11, "type_name"=>"SbsApi.Device.ListServicesResponse.AddLink"}
@property (nonatomic, strong) NSArray<SBSApiDevice_ListServicesResponse_AddLinkDTO*> *add_links;
@end

@interface SBSApiDevice_DisconnectServiceRequestDTO : BaseDTO
// {"name"=>"id", "number"=>1, "label"=>2, "type"=>9}
@property (nonatomic, strong) NSString *_id;
@end

@interface SBSApiDevice_DisconnectServiceResponseDTO : BaseDTO
// {"name"=>"error", "number"=>1, "label"=>1, "type"=>9}
@property (nonatomic, strong) NSString *error;
@end

@interface SBSApiUser_AuthenticateRequestDTO : BaseDTO
// {"name"=>"username", "number"=>1, "label"=>2, "type"=>9}
@property (nonatomic, strong) NSString *username;
// {"name"=>"password", "number"=>2, "label"=>1, "type"=>9}
@property (nonatomic, strong) NSString *password;
// {"name"=>"create_account", "number"=>3, "label"=>1, "type"=>8}
@property (nonatomic, strong) NSNumber *create_account;
// {"name"=>"email", "number"=>4, "label"=>1, "type"=>9}
@property (nonatomic, strong) NSString *email;
@end

@interface SBSApiUser_LoginRequestDTO : BaseDTO
// {"name"=>"username", "number"=>1, "label"=>2, "type"=>9}
@property (nonatomic, strong) NSString *username;
// {"name"=>"password", "number"=>2, "label"=>2, "type"=>9}
@property (nonatomic, strong) NSString *password;
@end

@interface SBSApiUser_RegisterRequestDTO : BaseDTO
// {"name"=>"username", "number"=>1, "label"=>2, "type"=>9}
@property (nonatomic, strong) NSString *username;
// {"name"=>"password", "number"=>2, "label"=>2, "type"=>9}
@property (nonatomic, strong) NSString *password;
// {"name"=>"email", "number"=>3, "label"=>2, "type"=>9}
@property (nonatomic, strong) NSString *email;
@end

@interface SBSApiUser_AuthenticateResponseDTO : BaseDTO
// {"name"=>"user", "number"=>1, "label"=>1, "type"=>11, "type_name"=>"SbsPb.UserProto"}
@property (nonatomic, strong) SBSUserProtoDTO *user;
// {"name"=>"auth_token", "number"=>2, "label"=>1, "type"=>9}
@property (nonatomic, strong) NSString *auth_token;
// {"name"=>"channel_name", "number"=>3, "label"=>1, "type"=>9}
@property (nonatomic, strong) NSString *channel_name;
// {"name"=>"error_message", "number"=>4, "label"=>1, "type"=>9}
@property (nonatomic, strong) NSString *error_message;
@end

@interface SBSApiUser_MeRequestDTO : BaseDTO
@end

@interface SBSApiUser_MeResponseDTO : BaseDTO
// {"name"=>"user", "number"=>1, "label"=>1, "type"=>11, "type_name"=>"SbsPb.UserProto"}
@property (nonatomic, strong) SBSUserProtoDTO *user;
// {"name"=>"connected_services", "number"=>2, "label"=>3, "type"=>11, "type_name"=>"SbsPb.ConnectedService"}
@property (nonatomic, strong) NSArray<SBSConnectedServiceDTO*> *connected_services;
@end

@interface SBSApiConversation_ListResponse_ConversationDataDTO : BaseDTO
// {"name"=>"conversation", "number"=>1, "label"=>2, "type"=>11, "type_name"=>"SbsPb.ConversationProto"}
@property (nonatomic, strong) SBSConversationProtoDTO *conversation;
// {"name"=>"last_message", "number"=>2, "label"=>1, "type"=>11, "type_name"=>"SbsPb.MessageProto"}
@property (nonatomic, strong) SBSMessageProtoDTO *last_message;
@end

@interface SBSApiConversation_GetFeedbacksResponse_FeedbackDTO : BaseDTO
// {"name"=>"feedback_id", "number"=>1, "label"=>2, "type"=>9}
@property (nonatomic, strong) NSString *feedback_id;
// {"name"=>"message_id", "number"=>2, "label"=>2, "type"=>9}
@property (nonatomic, strong) NSString *message_id;
// {"name"=>"text", "number"=>3, "label"=>2, "type"=>9}
@property (nonatomic, strong) NSString *text;
// {"name"=>"parses", "number"=>4, "label"=>3, "type"=>11, "type_name"=>"SbsApi.Conversation.GetFeedbacksResponse.Feedback.ParsedMessage"}
@property (nonatomic, strong) NSArray<SBSApiConversation_GetFeedbacksResponse_Feedback_ParsedMessageDTO*> *parses;
// {"name"=>"accepted_parse", "number"=>5, "label"=>1, "type"=>11, "type_name"=>"SbsApi.Conversation.GetFeedbacksResponse.Feedback.ParsedMessage"}
@property (nonatomic, strong) SBSApiConversation_GetFeedbacksResponse_Feedback_ParsedMessageDTO *accepted_parse;
@end

@interface SBSApiDevice_ListServicesResponse_AddLinkDTO : BaseDTO
// {"name"=>"url", "number"=>1, "label"=>2, "type"=>9}
@property (nonatomic, strong) NSString *url;
// {"name"=>"text", "number"=>2, "label"=>2, "type"=>9}
@property (nonatomic, strong) NSString *text;
// {"name"=>"kind", "number"=>3, "label"=>2, "type"=>9}
@property (nonatomic, strong) NSString *kind;
// {"name"=>"device_service", "number"=>4, "label"=>2, "type"=>11, "type_name"=>"SbsPb.DeviceService"}
@property (nonatomic, strong) SBSDeviceServiceDTO *device_service;
@end

@interface SBSApiConversation_GetFeedbacksResponse_Feedback_ParsedMessageDTO : BaseDTO
// {"name"=>"intent_name", "number"=>1, "label"=>2, "type"=>9}
@property (nonatomic, strong) NSString *intent_name;
// {"name"=>"confidence", "number"=>2, "label"=>2, "type"=>2}
@property (nonatomic, strong) NSNumber *confidence;
// {"name"=>"variables", "number"=>3, "label"=>3, "type"=>11, "type_name"=>"SbsApi.Conversation.ParsedMessageVar"}
@property (nonatomic, strong) NSArray<SBSApiConversation_ParsedMessageVarDTO*> *variables;
@end
