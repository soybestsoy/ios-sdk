//
//  MessageWrapper.m
//  Pods
//
//  Created by Peter Hsu on 2/4/16.
//
//

#import "MessageWrapper.h"

#import "DownloadImageView.h"

@interface MessageWrapper()

@property (nonatomic, strong) DownloadImageView *downloadImageView;

@end

@implementation MessageWrapper


@synthesize messageProto;

- (id)initWithMessage:(SBSMessageProtoDTO *)message {
    
    self = [super init];
    
    if (self) {
        self.messageProto = message;
    }
    
    return self;
}
/**
 *  @return A string identifier that uniquely identifies the user who sent the message.
 *
 *  @discussion If you need to generate a unique identifier, consider using
 *  `[[NSProcessInfo processInfo] globallyUniqueString]`
 *
 *  @warning You must not return `nil` from this method. This value must be unique.
 */
- (NSString *)senderId {
    return [messageProto.user._id isEqualToString:@"0"] ? SENDER_ID_SOY : SENDER_ID_ME;
}

/**
 *  @return The display name for the user who sent the message.
 *
 *  @warning You must not return `nil` from this method.
 */
- (NSString *)senderDisplayName {
    return @"";
}

/**
 *  @return The date that the message was sent.
 *
 *  @warning You must not return `nil` from this method.
 */
- (NSDate *)date {
    return [NSDate dateWithTimeIntervalSinceNow:[messageProto.post_mts doubleValue]/1000];
}

/**
 *  This method is used to determine if the message data item contains text or media.
 *  If this method returns `YES`, an instance of `JSQMessagesViewController` will ignore
 *  the `text` method of this protocol when dequeuing a `JSQMessagesCollectionViewCell`
 *  and only call the `media` method.
 *
 *  Similarly, if this method returns `NO` then the `media` method will be ignored and
 *  and only the `text` method will be called.
 *
 *  @return A boolean value specifying whether or not this is a media message or a text message.
 *  Return `YES` if this item is a media message, and `NO` if it is a text message.
 */
- (BOOL)isMediaMessage {
    return [messageProto.kind intValue] == MediaKindImage;
}

/**
 *  @return An integer that can be used as a table address in a hash table structure.
 *
 *  @discussion This value must be unique for each message with distinct contents.
 *  This value is used to cache layout information in the collection view.
 */
- (NSUInteger)messageHash {
    return [messageProto._id hash];
}

- (NSString *)text {
    return messageProto.text;
}

/**
 *  @return The media item of the message.
 *
 *  @warning You must not return `nil` from this method.
 */
- (id<JSQMessageMediaData>)media {
    return self;
}


#pragma mark - JSQMessageMediaData

/**
 *  @return An initialized `UIView` object that represents the data for this media object.
 *
 *  @discussion You may return `nil` from this method while the media data is being downloaded.
 */
- (UIView *)mediaView {
    return [self downloadImageView];
}

- (UIImageView *)imageView {
    return [[self downloadImageView] imageView];
}

- (DownloadImageView *)downloadImageView {
    if (_downloadImageView == nil) {
        _downloadImageView = [[DownloadImageView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
        
        [_downloadImageView setUrl:messageProto.image_url];
        
    }
    return _downloadImageView;
}

/**
 *  @return The frame size for the mediaView when displayed in a `JSQMessagesCollectionViewCell`.
 *
 *  @discussion You should return an appropriate size value to be set for the mediaView's frame
 *  based on the contents of the view, and the frame and layout of the `JSQMessagesCollectionViewCell`
 *  in which mediaView will be displayed.
 *
 *  @warning You must return a size with non-zero, positive width and height values.
 */
- (CGSize)mediaViewDisplaySize {
    return CGSizeMake(100, 100);
}

/**
 *  @return A placeholder media view to be displayed if mediaView is not yet available, or `nil`.
 *  For example, if mediaView will be constructed based on media data that must be downloaded,
 *  this placeholder view will be used until mediaView is not `nil`.
 *
 *  @discussion If you do not need support for a placeholder view, then you may simply return the
 *  same value here as mediaView. Otherwise, consider using `JSQMessagesMediaPlaceholderView`.
 *
 *  @warning You must not return `nil` from this method.
 *
 *  @see JSQMessagesMediaPlaceholderView.
 */
- (UIView *)mediaPlaceholderView {
    return nil;
}

/**
 *  @return An integer that can be used as a table address in a hash table structure.
 *
 *  @discussion This value must be unique for each media item with distinct contents.
 *  This value is used to cache layout information in the collection view.
 */
- (NSUInteger)mediaHash {
    return [self.messageProto._id hash];
}


@end
