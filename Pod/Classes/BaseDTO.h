//
//  BaseDTO.h
//  Pods
//
//  Created by Peter Hsu on 1/29/16.
//
//

#import <Foundation/Foundation.h>

@interface BaseDTO : NSObject
- (id)initWithDictionary:(id)dictionary;
- (NSDictionary *)params;
- (id)deserializeFromJson:(NSDictionary*)json key:(NSString*)key class:(Class)clazz repeated:(BOOL)repeated optional:(BOOL)optional;
- (void)serializeToJson:(NSDictionary*)json key:(NSString*)key value:(id)value class:(Class)clazz repeated:(BOOL)repeated optional:(BOOL)optional;
@end
