//
//  SpeechRecognizer.m
//  SoySDK
//
//  Created by Peter Hsu on 4/26/16.
//  Copyright © 2016 Peter Hsu. All rights reserved.
//

#import "SpeechRecognizer.h"



#define SKSURL @"nmsps://NMDPPRODUCTION_Hsiu_Fan_Wang_Soy_20160311183504@hri.nmdp.nuancemobility.net:443"
#define SKSKey @"7d31aaa653f59fa81653309330bf3092b19f6b6d66a0171e5fab707ed01b354d4cdff49fd49347eae6777e1737aea9e84d1456b3b56dd114194192e95cc736a9"

@interface SpeechRecognizer ()

@property (nonatomic, strong) SKSession *session;
@property (nonatomic, strong) SKTransaction *recognizeTransaction;

@end
@implementation SpeechRecognizer

@synthesize delegate;

- (id)init {
    self = [super init];
    
    if (self) {
        _session = [[SKSession alloc] initWithURL:[NSURL URLWithString:SKSURL]
                                         appToken:SKSKey];
        
    }
    
    return self;
}

- (void)startSpeechRecognition {
    if (_recognizeTransaction != nil) {
        
        // recognition already happening
        return;
    }
    _recognizeTransaction = [_session recognizeWithType:SKTransactionSpeechTypeDictation
                                              detection:SKTransactionEndOfSpeechDetectionShort
                                               language:@"eng-USA"
                                               delegate:self];
    
    
}

- (void)cancelSession {
    if (_recognizeTransaction != nil) {
        [_recognizeTransaction cancel];
    }
}

#pragma mark - SKTransactionDelegate

/*!
 @abstract Called when the transaction starts recording audio.
 */
- (void)transactionDidBeginRecording:(SKTransaction *)transaction {
    
    if (self.delegate != nil) {
        [self.delegate didSpeechRecognitionStart];
    }
}

/*!
 @abstract Called when the transaction stops recording audio.
 */
- (void)transactionDidFinishRecording:(SKTransaction *)transaction {
    _recognizeTransaction = nil;
}

/*!
 @abstract Called when the transaction receives a text recognition.
 @discussion This delegate method may be called multiple times depending on the specific recognition operation and options.  In a standard one-shot recognition this method will be called once after recording completes and the text is successfully recognized.  When using progressive dictation results, this method may be called multiple times before recording completes.  Each successive recognition will replace any previously received recognition with the same chunk ID.
 */
- (void)transaction:(SKTransaction *)transaction didReceiveRecognition:(SKRecognition *)recognition {
    
    if (self.delegate != nil) {
        [self.delegate didSpeechRecognitionReceiveTextInput:recognition.text];
    }
}

/*!
 @abstract Called when the transaction completes successfully.
 @discussion This method is called when the transaction has completed successfully; all recognition responses, interpretations, application responses and audio buffers have been fully received.  The suggestion is a string with helpful user text that describes how the user may improve recognition in future transactions.  This suggestion will only be present if the recognition is sub-optimal and may be `nil` when there are no problems.  This method or transaction:didFailWithError:suggestion: will always be called, but never both.
 */
- (void)transaction:(SKTransaction *)transaction didFinishWithSuggestion:(NSString *)suggestion {
    
    _recognizeTransaction = nil;
    
    if (self.delegate != nil) {
        [self.delegate didSpeechRecognitionFinish];
    }

}

/*!
 @abstract Called when the transaction has an error.
 @discussion This method is called when the recognition process results in an error due to any number of circumstances.  The audio system may fail to initialize, the server connection may be disrupted or a parameter specified during initialization, such as language or authentication information was invalid.  This method will still be called in the case that multiple responses are returned and an error occurs before the transaction completes.  This method or transaction:didFinishWithSuggestion: will always be called, but never both.
 */
- (void)transaction:(SKTransaction *)transaction didFailWithError:(NSError *)error suggestion:(NSString *)suggestion {
    
    _recognizeTransaction = nil;
    if (self.delegate != nil) {
        [self.delegate didSpeechRecognitionFinishWithError:error];
    }

}


@end
