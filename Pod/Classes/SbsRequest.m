//
//  SbsRequest.m
//  Pods
//
//  Created by Peter Hsu on 1/29/16.
//
//

#import "SbsRequest.h"

@implementation SbsRequest

@synthesize endpoint;
@synthesize httpMethod;
@synthesize responseClass;

- (id)initWithEndpoint:(NSString *)e
            parameters:(NSDictionary *)p
            httpMethod:(NSString *)h
         responseClass:(Class)r {
    self = [super init];
    
    if (self) {
        self.endpoint = e;
        self.parameters = p;
        self.httpMethod = h;
        self.responseClass = r;
    }
    
    return self;
}

@end
