//
//  SbsNetwork.m
//  Pods
//
//  Created by Peter Hsu on 1/29/16.
//
//

#import "SbsNetwork.h"
#import "SessionManager.h"

@interface SbsNetwork ()

@property (nonatomic, strong) NSOperationQueue *queue;
@property (nonatomic, strong) NSString *baseURL;

@end


@implementation SbsNetwork

- (id)initWithBaseUrl:(NSString *)b {
    
    self = [super init];
    
    if (self) {
        _queue = [[NSOperationQueue alloc] init];
        [_queue setName:@"com.soychat.network"];
        
        _baseURL = b;
    }
    
    return self;
}


- (void)enqueueRequest:(SbsRequest *)request
               handler:(SbsResponseHandler)handler {
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", _baseURL, request.endpoint]];
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    
    urlRequest.HTTPMethod = request.httpMethod;
    
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    NSString *authToken = [SessionManager sharedManager].currentSession.authToken;
    if (authToken != nil) {
        [urlRequest addValue:[NSString stringWithFormat:@"Bearer %@", authToken]
          forHTTPHeaderField:@"Authorization"];
    }
    
    NSString *apiKey = [SessionManager sharedManager].apiKey;
    if (apiKey != nil) {
        [urlRequest addValue:apiKey
          forHTTPHeaderField:@"X-API-Key"];
    }
    
    if ([[request.httpMethod uppercaseString] isEqualToString:@"POST"]) {
        
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:request.parameters
                                                           options:0
                                                             error:&error];

        if (error != nil) {
            handler(nil, error);
            return;
        }
        
        [urlRequest setHTTPBody:jsonData];

    } else {
        // TODO: encode parameters in URL for GET requests
    }
    
    
    [NSURLConnection sendAsynchronousRequest:urlRequest
                                       queue:_queue
                           completionHandler:^(NSURLResponse* response,
                                               NSData* data,
                                               NSError* error)
    {
        if (data) {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
            if (httpResponse.statusCode == 200) {
                NSError *error;
                
//                NSString *string = [[NSString alloc] initWithData:data
//                                                         encoding:NSUTF8StringEncoding];
                
                id jsonObject = [NSJSONSerialization JSONObjectWithData:data
                                                                options:0
                                                                  error:&error];
                
                // errors may come back as a 200.. search for the
                // "error_message" key that signifies it is a failed request
                if ([jsonObject isKindOfClass:NSDictionary.class] &&
                    [jsonObject valueForKey:@"error_message"] != nil) {
                    NSError *error = [NSError errorWithDomain:[jsonObject valueForKey:@"error_message"]
                                                         code:100
                                                     userInfo:nil];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        handler(nil, error);
                    });
                } else if (jsonObject) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        Class class = request.responseClass;
                        BaseDTO *dto = [[class alloc] initWithDictionary:jsonObject];
                        
                        handler(dto, error);
                    });
                } else {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        handler(nil, error);
                    });
                }
            }
            else {
                NSString* desc = [[NSString alloc] initWithFormat:@"(%d) %@",
                                  (int)(httpResponse.statusCode),
                                  [NSHTTPURLResponse localizedStringForStatusCode:httpResponse.statusCode]];
                NSError* error = [NSError errorWithDomain:@"HTTP Request"
                                                     code:-1000
                                                 userInfo:@{NSLocalizedDescriptionKey: desc}];
                dispatch_async(dispatch_get_main_queue(), ^{
                    handler(nil, error);
                });
            }
        }
        else {
            // request failed - error contains info about the failure
            dispatch_async(dispatch_get_main_queue(), ^{
                handler(nil, error);
            });
        }
    }];
    
    
}

@end
