//
//  SoyChatViewController.h
//  Pods
//
//  Created by Peter Hsu on 2/4/16.
//
//

#import <UIKit/UIKit.h>

#import <JSQMessagesViewController/JSQMessages.h>

#import "MessageWrapper.h"
#import "SbsApiClient.h"
#import "ChatManager.h"
#import "SessionManager.h"
#import "SpeechRecognizer.h"


@interface SoyChatViewController : JSQMessagesViewController <ChatDelegate, SessionManagerDelegate, SpeechRecognizerDelegate>

- (void)addPreviousMessages:(NSArray <SBSMessageProtoDTO *> *)messages;
- (void)addNewMessage:(SBSMessageProtoDTO *)message;

@end
