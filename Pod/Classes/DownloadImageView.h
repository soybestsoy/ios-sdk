//
//  DownloadImageView.h
//  Pods
//
//  Created by Peter Hsu on 2/4/16.
//
//

#import <UIKit/UIKit.h>

@interface DownloadImageView : UIView

- (void)setUrl:(NSString *)url;
- (UIImageView *)imageView;

@end
