//
//  SessionManager.m
//  Pods
//
//  Created by Peter Hsu on 2/2/16.
//
//

#import "SessionManager.h"

#import "SbsProtoDTO.h"
#import "SbsApiClient.h"

@implementation SessionManager

@synthesize currentSession;
@synthesize apiClient;
@synthesize apiKey;
@synthesize delegate;

static SessionManager *sharedSessionManager = nil;

+ (SessionManager *)sharedManager {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedSessionManager = [[self alloc] init];
    });
    return sharedSessionManager;
}

- (BOOL)isAuthenticated {
    return self.currentSession != nil;
}

- (void)authenticate:(NSString *)username
            password:(NSString *)password
         create_user:(BOOL)create_user
          completion:(void (^)(SBSApiUser_AuthenticateResponseDTO *response, NSError *error))completion {
    
    SBSApiUser_AuthenticateRequestDTO *request = [[SBSApiUser_AuthenticateRequestDTO alloc] init];
    request.username = username;
    request.password = password;
    request.create_account = [NSNumber numberWithBool:create_user];

    [[SbsApiClient sharedClient] user_Authenticate:request
                                        completion:^(SBSApiUser_AuthenticateResponseDTO *response, NSError *error)
     {
         if (error == nil) {
             [self startSession:response];
             
             if (delegate != nil) {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [delegate onSessionStarted];
                 });
             }
         } else {
             dispatch_async(dispatch_get_main_queue(), ^{
                 [delegate onSessionFailed];
             });
         }
         completion(response, error);
        
    }];
}

- (void)startSession:(SBSApiUser_AuthenticateResponseDTO *)response {

    SbsSession *session = [[SbsSession alloc] init];
    
    session.authToken = response.auth_token;
    session.channelName = response.channel_name;
    session.user = response.user;
    
    
    self.currentSession = session;

}

@end
