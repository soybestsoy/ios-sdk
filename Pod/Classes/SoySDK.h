//
//  SoySDK.h
//  Pods
//
//  Created by Peter Hsu on 1/27/16.
//
//

#import <Foundation/Foundation.h>

#import "SoyChatViewController.h"
#import "SoySessionStatus.h"

// subscribe to this with nil object and receive a SoySessionStatus object

extern NSString *const SoySessionStatusNotification;

@interface SoySDK : NSObject

+ (void)init:(NSString *)apiKey;
+ (void)setUser:(NSString *)username secret:(NSString *)secret;
+ (SoyChatViewController *)getSoyChatViewController;


@end
