//
//  SoySessionStatus.h
//  Pods
//
//  Created by Peter Hsu on 2/15/16.
//
//

#import <Foundation/Foundation.h>

typedef enum {
    SessionStatusUnauthenticated,
    SessionStatusAuthenticated,
} SessionStatus;

@interface SoySessionStatus : NSObject

@property (nonatomic) SessionStatus status;
@property (nonatomic, strong) NSError *error;

@end
