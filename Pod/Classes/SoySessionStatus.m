//
//  SoySessionStatus.m
//  Pods
//
//  Created by Peter Hsu on 2/15/16.
//
//

#import "SoySessionStatus.h"

@implementation SoySessionStatus

@synthesize status;
@synthesize error;

+ (NSString *)stringForStatus:(SessionStatus)status {
    switch (status) {
        case SessionStatusAuthenticated:
            return @"Authenticated";
            
        case SessionStatusUnauthenticated:
            return @"Not Authenticated";
            
        default:
            return @"Unknown";
    }
}
- (NSString *)description {
    
    return [NSString stringWithFormat:@"status=%@, error=%@", [SoySessionStatus stringForStatus:status], [error localizedDescription]];
}

@end
