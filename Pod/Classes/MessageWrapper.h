//
//  MessageWrapper.h
//  Pods
//
//  Created by Peter Hsu on 2/4/16.
//
//

#import <Foundation/Foundation.h>

#import <JSQMessagesViewController/JSQMessages.h>

#import "SBSProtoDTO.h"

#define SENDER_ID_SOY @"soy"
#define SENDER_ID_ME  @"me"

@interface MessageWrapper : NSObject <JSQMessageData, JSQMessageMediaData>

- (id)initWithMessage:(SBSMessageProtoDTO *)message;

- (UIImageView *)imageView;

@property (nonatomic, strong) SBSMessageProtoDTO *messageProto;

@end
