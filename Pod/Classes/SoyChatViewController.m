//
//  SoyChatViewController.m
//  Pods
//
//  Created by Peter Hsu on 2/4/16.
//
//

#import "SoyChatViewController.h"

#import <AVFoundation/AVFoundation.h>

#import "SoySDK.h"

#import "FullScreenImageViewController.h"

#import "SpeechRecognizer.h"

@interface SoyChatViewController ()

@property (nonatomic, strong) NSMutableArray <MessageWrapper *> *messages;
@property (nonatomic, strong) NSMutableSet <NSString *> *messageIds;

@property (nonatomic, strong) NSMutableDictionary <NSNumber *, UIImageView *> *imagesByIndex;

@property (nonatomic, strong) UIRefreshControl *refreshControl;
@property (nonatomic) BOOL refreshing;
@property (nonatomic) double firstTimestamp;

@property (nonatomic) BOOL initialFetchSucceeded;

@property (nonatomic, strong) SpeechRecognizer *speechRecognizer;
@property (nonatomic, strong) UIView *speechOverlayView;
@property (nonatomic, strong) UITextView *speechOverlayTextView;

@end

@implementation SoyChatViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _speechRecognizer = [[SpeechRecognizer alloc] init];
    _speechRecognizer.delegate = self;
    
    _messages = [NSMutableArray array];
    _messageIds = [NSMutableSet set];
    _imagesByIndex = [NSMutableDictionary dictionary];
    
    [self setSenderId:SENDER_ID_ME];
    [self setSenderDisplayName:@""];
    
    self.inputToolbar.contentView.leftBarButtonItem = nil;
    self.inputToolbar.contentView.textView.placeHolder = @"Ask Soy something...";
    
    _refreshControl = [[UIRefreshControl alloc] init];
    [_refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.collectionView addSubview:_refreshControl];
    
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0,0,30,30);

    NSBundle *bundle = [NSBundle bundleForClass:[SoySDK class]];
    UIImage *image = [UIImage imageNamed:@"soylib_microphone" inBundle:bundle compatibleWithTraitCollection:nil];
    
    button.imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    [button setImage:image forState:UIControlStateNormal];
    
    // This button will call the `didPressAccessoryButton:` selector on your JSQMessagesViewController subclass
    self.inputToolbar.contentView.leftBarButtonItem = button;
    
    // The library will call the correct selector for each button, based on this value
    self.inputToolbar.sendButtonOnRight = YES;
}


- (void)onSessionStarted {
    [self initialFetch];
    [self subscribeToChannel];
}

- (void)onSessionFailed {
    NSLog(@"Error loading messages");
    
    // TODO: allow retry?
}

- (void)subscribeToChannel {
    if (![[SessionManager sharedManager] isAuthenticated]) {
        return;
    }
    
    NSString *channelName = [SessionManager sharedManager].currentSession.channelName;
    
    [ChatManager sharedManager].delegate = self;
    [[ChatManager sharedManager] subscribeToChannel:channelName];

}

- (void)initialFetch {
    
    
    if (![[SessionManager sharedManager] isAuthenticated]) {
        [SessionManager sharedManager].delegate = self;
        NSLog(@"Not authenticated yet, subscribing as delegate");
        return;
    }
    if (_initialFetchSucceeded) {
        // already fetched
        return;
    }
    
    // initial fetch
    [[ChatManager sharedManager] getMessages:nil
                              startTimestamp:0
                                endTimestamp:[[NSDate date] timeIntervalSince1970] completion:^(NSArray *messages, BOOL endOfMessages, NSError *error) {
                                    
                                    if (error == nil) {
                                        _initialFetchSucceeded = YES;
                                    }
                                    [self addPreviousMessages:messages];
                                    [self scrollToBottomAnimated:YES];
                                }];

}

- (void)refresh:(UIRefreshControl *)refreshControl {
    
    if (!_initialFetchSucceeded) {
        [self initialFetch];
        return;
    }
    
    // Do your job, when done:
    @synchronized(self) {
        if (_refreshing) {
            return;
        }
    }
    _refreshing = YES;
    
    double endTimestamp = 0;
    if (_firstTimestamp == 0) {
        endTimestamp = [[NSDate date] timeIntervalSince1970];
    } else {
        endTimestamp = _firstTimestamp;
    }
    
    
    [[ChatManager sharedManager] getMessages:nil
                              startTimestamp:0
                                endTimestamp:endTimestamp
                                  completion:^(NSArray *messages, BOOL endOfMessages, NSError *error)
     {
         
         [self endRefreshing:nil];
         
         if (error != nil) {
             // TODO: handle error
             NSLog(@"Error fetching messages %@", error);
             
             return;
         }
         
         NSInteger messageCount = [messages count];
         
         if (messageCount > 0) {
             [self addPreviousMessages:messages];
             
             // scroll to first new message
             [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:messageCount-1 inSection:0]
                                         atScrollPosition:UICollectionViewScrollPositionTop animated:NO];
         } else {
             // reached end of messages
             // TODO: special handling
         }
     }];
    
    
}

- (void)endRefreshing:(id)sender {
    _refreshing = NO;
    
    [_refreshControl endRefreshing];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self initialFetch];
    [self subscribeToChannel];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];

    [[ChatManager sharedManager] unsubscribe];

    
}

- (void)didReceiveMessage:(SBSMessageProtoDTO *)message {
    [self addNewMessage:message];
    
    if ([message.post_mts doubleValue]/1000 < _firstTimestamp || _firstTimestamp == 0) {
        _firstTimestamp = [message.post_mts doubleValue]/1000;
    }
}

- (void)insertMessage:(SBSMessageProtoDTO *)message lastMessage:(BOOL)lastMessage {
    
    if ([_messageIds containsObject:message._id]) {
        // duplicate
        return;
    }
    
    NSUInteger targetIndex = [_messages count];
    
    if (lastMessage) {
        targetIndex = [_messages count];
    } else {
        for (int i=0; i<[_messages count]; i++) {
            SBSMessageProtoDTO *oldMessage = [_messages objectAtIndex:i].messageProto;
            
            if ([oldMessage.post_mts doubleValue] > [message.post_mts doubleValue]) {
                targetIndex = i;
                break;
            }
        }
    }
    
    [_messages insertObject:[[MessageWrapper alloc] initWithMessage:message] atIndex:targetIndex];
    [_messageIds addObject:message._id];
    
    if ([message.post_mts doubleValue]/1000 < _firstTimestamp || _firstTimestamp == 0) {
        _firstTimestamp = [message.post_mts doubleValue]/1000;
    }

    [self.collectionView reloadData];
    
}

- (void)addPreviousMessages:(NSArray <SBSMessageProtoDTO *> *)messages {
    
    for (SBSMessageProtoDTO *message in messages) {
        [self insertMessage:message lastMessage:NO];
    }
    [self.collectionView reloadData];
    
}

- (void)addNewMessage:(SBSMessageProtoDTO *)message {
    
    [self insertMessage:message lastMessage:YES];
    
    [self scrollToBottomAnimated:YES];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)startSpeechRecognition {
    [_speechRecognizer startSpeechRecognition];
}

#pragma mark - JSQMessagesViewController method overrides

- (void)didPressSendButton:(UIButton *)button
           withMessageText:(NSString *)text
                  senderId:(NSString *)senderId
         senderDisplayName:(NSString *)senderDisplayName
                      date:(NSDate *)date {
    
    [[ChatManager sharedManager] sendMessage:text
                                  completion:^(SBSMessageProtoDTO *message, NSError *error) {
                                      
                                      if (error == nil) {
                                          
                                          [self addNewMessage:message];
                                          [self finishSendingMessageAnimated:YES];
                                          
                                      } else {
                                          // TODO: show alert for error
                                          NSLog(@"Error sending message %@", error);
                                      }
                                      
                                  }];
    
}

- (void)didPressAccessoryButton:(UIButton *)sender {
    
    [self startSpeechRecognition];
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
}

#pragma mark - JSQMessages delegate 

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapMessageBubbleAtIndexPath:(NSIndexPath *)indexPath {
    MessageWrapper *wrapper = [_messages objectAtIndex:indexPath.row];
    
    if ([wrapper.messageProto.kind intValue] == MediaKindImage) {
        // ensure image is loaded
        if ([wrapper imageView].image != nil) {
            
            FullScreenImageViewController *vc = [[FullScreenImageViewController alloc] initWithImage:[wrapper imageView].image];
            
            [self presentViewController:vc
                               animated:YES
                             completion:^{
                                 
                             }];
        }
    } else if ([wrapper.messageProto.kind intValue] == MediaKindText) {
        [self speakText:wrapper.messageProto.text];
    }
}




#pragma mark - JSQMessages collection view data source

- (id<JSQMessageData>)collectionView:(JSQMessagesCollectionView *)collectionView messageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [_messages objectAtIndex:indexPath.row];
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didDeleteMessageAtIndexPath:(NSIndexPath *)indexPath
{
    
//    NSAssert(NO, @"ERROR: required method not implemented: %s", __PRETTY_FUNCTION__);
}

- (id<JSQMessageBubbleImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView messageBubbleImageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    JSQMessagesBubbleImageFactory *bubbleFactory = [[JSQMessagesBubbleImageFactory alloc] init];
    
    MessageWrapper *wrapper = [_messages objectAtIndex:indexPath.row];
    
    if ([wrapper.messageProto.user._id isEqualToString:@"0"]) {
        return [bubbleFactory outgoingMessagesBubbleImageWithColor:[UIColor jsq_messageBubbleGreenColor]];
    } else {
        return [bubbleFactory outgoingMessagesBubbleImageWithColor:[UIColor jsq_messageBubbleLightGrayColor]];
    }
}

- (id<JSQMessageAvatarImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView avatarImageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return nil;
}

#pragma mark - UICollectionView DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [_messages count];
}


- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath {
    return kJSQMessagesCollectionViewCellLabelHeightDefault;
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath {
    MessageWrapper *wrapper = [_messages objectAtIndex:indexPath.row];
    
    NSDate *messageDate = [NSDate dateWithTimeIntervalSince1970:[wrapper.messageProto.post_mts doubleValue]/1000];
    return [[JSQMessagesTimestampFormatter sharedFormatter] attributedTimestampForDate:messageDate];
}


#pragma mark - SpeechRecognizerDelegate

- (void)didSpeechRecognitionStart {
    
    [self showSpeechOverlayView];
    
}

- (void)didSpeechRecognitionFinish {
    
}

- (void)didSpeechRecognitionFinishWithError:(NSError *)error {
    [self hideSpeechOverlayView];
}

- (void)didSpeechRecognitionReceiveTextInput:(NSString *)text {
    self.inputToolbar.contentView.textView.text = text;
    
    _speechOverlayTextView.text = text;
    
    
    if ([text length] > 0) {
        [self performSelector:@selector(sendSpeechRecognitionText:) withObject:text afterDelay:0.2f];
    } else {
        [self hideSpeechOverlayView];
    }
    
}

- (void)sendSpeechRecognitionText:(NSString *)text {
    [self hideSpeechOverlayView];
    
    [self didPressSendButton:nil
             withMessageText:text
                    senderId:nil
           senderDisplayName:nil
                        date:[NSDate date]];
}


#pragma mark - Overlay screen

- (void)didCancelSpeechRecognition:(id)sender {
    [_speechRecognizer cancelSession];
}

- (void)showSpeechOverlayView {
    [self showSpeechOverlayView:@"Listening..."];
}

- (void)showSpeechOverlayView:(NSString *)text {
    if (_speechOverlayView == nil) {
        UIView *view = [[UIView alloc] initWithFrame:self.view.bounds];
        
        // catch touches while shown
        view.userInteractionEnabled = YES;
        
        view.backgroundColor = [UIColor colorWithWhite:0 alpha:0.8f];
        
        _speechOverlayView = view;
        
        UIButton *cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        cancelButton.frame = CGRectMake(0, 0, view.frame.size.width, view.frame.size.height);
        [cancelButton addTarget:self action:@selector(didCancelSpeechRecognition:) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:cancelButton];
        
        int bottomBarHeight = 120;
        
        UIView *whiteBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, view.frame.size.height - bottomBarHeight, view.frame.size.width, bottomBarHeight)];
        whiteBackgroundView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleWidth;
        whiteBackgroundView.backgroundColor = [UIColor whiteColor];
        whiteBackgroundView.userInteractionEnabled = YES; // prevent taps from hitting cancel button
        
        [view addSubview:whiteBackgroundView];
        
        
        int logoSize = bottomBarHeight * 0.8f;
        
        NSBundle *bundle = [NSBundle bundleForClass:[SoySDK class]];
        UIImage *image = [UIImage imageNamed:@"soylib_logo_whitecircle" inBundle:bundle compatibleWithTraitCollection:nil];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(view.frame.size.width/2 - logoSize/2, view.frame.size.height - bottomBarHeight - logoSize/2, logoSize, logoSize)];
        
        imageView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin;
        imageView.image = image;
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        
        [view addSubview:imageView];

        
        
        UITextView *textView = [[UITextView alloc] initWithFrame:CGRectMake(0, view.frame.size.height - bottomBarHeight/2, view.frame.size.width, 30)];
        textView.tag = 1000;
        textView.font = [UIFont systemFontOfSize:16];
        textView.textAlignment = NSTextAlignmentCenter;
        textView.backgroundColor = [UIColor clearColor];
        textView.textColor = [UIColor blackColor];
        textView.editable = NO;
        textView.selectable = NO;
        textView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleWidth;
        [view addSubview:textView];
        
        textView.text = text;
        
        _speechOverlayTextView = textView;
        
    }
    
    [self.view addSubview:_speechOverlayView];
}


- (void)hideSpeechOverlayView {
    
    [_speechOverlayView removeFromSuperview];
    _speechOverlayView = nil;
    
}


#pragma mark - TTS

- (void)speakText:(NSString *)text {
    AVSpeechSynthesizer *synthesizer = [[AVSpeechSynthesizer alloc]init];
    AVSpeechUtterance *utterance = [AVSpeechUtterance speechUtteranceWithString:text];
    [utterance setVolume:0.7f];
    [synthesizer speakUtterance:utterance];
    
}



@end
