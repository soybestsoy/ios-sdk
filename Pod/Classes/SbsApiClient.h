//
// SbsApiClient.h
//

#import <Foundation/Foundation.h>
#import "SbsProtoDTO.h"

@interface SbsApiClient : NSObject

+ (SbsApiClient *)sharedClient;

- (void)conversation_List:(SBSApiConversation_ListRequestDTO *)request
    completion:(void (^)(SBSApiConversation_ListResponseDTO *response, NSError *error))completion;
- (void)conversation_Get:(SBSApiConversation_GetRequestDTO *)request
    completion:(void (^)(SBSApiConversation_GetResponseDTO *response, NSError *error))completion;
- (void)conversation_SendMessage:(SBSApiConversation_SendMessageRequestDTO *)request
    completion:(void (^)(SBSApiConversation_SendMessageResponseDTO *response, NSError *error))completion;
- (void)conversation_GetFeedbacks:(SBSApiConversation_GetFeedbacksRequestDTO *)request
    completion:(void (^)(SBSApiConversation_GetFeedbacksResponseDTO *response, NSError *error))completion;
- (void)conversation_UpdateFeedback:(SBSApiConversation_UpdateFeedbackRequestDTO *)request
    completion:(void (^)(SBSApiConversation_UpdateFeedbackResponseDTO *response, NSError *error))completion;
- (void)conversation_Report:(SBSApiConversation_ReportRequestDTO *)request
    completion:(void (^)(SBSApiConversation_ReportResponseDTO *response, NSError *error))completion;
- (void)device_ListServices:(SBSApiDevice_ListServicesRequestDTO *)request
    completion:(void (^)(SBSApiDevice_ListServicesResponseDTO *response, NSError *error))completion;
- (void)device_DisconnectService:(SBSApiDevice_DisconnectServiceRequestDTO *)request
    completion:(void (^)(SBSApiDevice_DisconnectServiceResponseDTO *response, NSError *error))completion;
- (void)user_Authenticate:(SBSApiUser_AuthenticateRequestDTO *)request
    completion:(void (^)(SBSApiUser_AuthenticateResponseDTO *response, NSError *error))completion;
- (void)user_Login:(SBSApiUser_LoginRequestDTO *)request
    completion:(void (^)(SBSApiUser_AuthenticateResponseDTO *response, NSError *error))completion;
- (void)user_Register:(SBSApiUser_RegisterRequestDTO *)request
    completion:(void (^)(SBSApiUser_AuthenticateResponseDTO *response, NSError *error))completion;
- (void)user_Me:(SBSApiUser_MeRequestDTO *)request
    completion:(void (^)(SBSApiUser_MeResponseDTO *response, NSError *error))completion;
@end
