//
//  SoyChatTableViewCell.m
//  Pods
//
//  Created by Peter Hsu on 2/4/16.
//
//

#import "SoyChatTableViewCell.h"

@implementation SoyChatTableViewCell

@synthesize imageView;
@synthesize messageLabel;
@synthesize timestampLabel;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
