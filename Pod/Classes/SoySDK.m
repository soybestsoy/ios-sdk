//
//  SoySDK.m
//  Pods
//
//  Created by Peter Hsu on 1/27/16.
//
//

#import "SoySDK.h"
#import "SbsApiClient.h"
#import "SessionManager.h"
#import "ChatManager.h"
#import "MessageWrapper.h"

NSString *const SoySessionStatusNotification = @"SoySessionStatus";

@interface SoySDK()

@property (nonatomic, strong) ChatManager *chatManager;
@property (nonatomic, strong) SessionManager *sessionManager;

@end

@implementation SoySDK


static SoySDK *sharedSdk = nil;
+ (SoySDK *)sharedClient {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedSdk = [[self alloc] init];
    });
    return sharedSdk;
}

+ (void)init:(NSString *)apiKey {
    [self sharedClient].sessionManager.apiKey = apiKey;
}

- (id)init {
    self = [super init];
    
    if (self) {
        _chatManager = [ChatManager sharedManager];
        _sessionManager = [SessionManager sharedManager];
    }
    
    return self;
}

- (void)connect:(NSString *)channel {
    [_chatManager subscribeToChannel:channel];

}


+ (void)setUser:(NSString *)username secret:(NSString *)secret {
    
    
    [sharedSdk.sessionManager authenticate:username
                                  password:secret
                               create_user:NO
                                completion:^(SBSApiUser_AuthenticateResponseDTO *response, NSError *error)
     {
         if (error != nil) {
             SoySessionStatus *status = [[SoySessionStatus alloc] init];
             status.error = error;
             status.status = SessionStatusUnauthenticated;
             
             [[NSNotificationCenter defaultCenter] postNotificationName:SoySessionStatusNotification
                                                                 object:status];
             return;
         }
         
         SoySessionStatus *status = [[SoySessionStatus alloc] init];
         status.status = SessionStatusAuthenticated;
         
         [[NSNotificationCenter defaultCenter] postNotificationName:SoySessionStatusNotification
                                                             object:status];
         
         [sharedSdk connect:response.channel_name];
         
     }];
    
}

+ (SoyChatViewController *)getSoyChatViewController {
    return [[SoyChatViewController alloc] init];
}

@end
