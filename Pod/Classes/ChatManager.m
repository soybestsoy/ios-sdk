//
//  ChatManager.m
//  Pods
//
//  Created by Peter Hsu on 2/2/16.
//
//

#import "ChatManager.h"

#import "SbsApiClient.h"

#define MESSAGE_EVENT_NAME  @"message"
#define MESSAGE_PAGE_SIZE  10

@interface ChatManager ()

@property (nonatomic, strong) PTPusher *client;
@property (nonatomic, strong) PTPusherChannel *channel;
@property (nonatomic, strong) NSString *channelName;
@property (nonatomic) BOOL connected;

@end

@implementation ChatManager

@synthesize delegate;

static ChatManager *sharedManager = nil;

+ (ChatManager *)sharedManager {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
    });
    return sharedManager;
}

- (id)init {
    
    self = [super init];
    
    if (self) {
        _client = [PTPusher pusherWithKey:@"d49c66f41432edab93d6" delegate:self];
        
        [_client connect];
    }
    
    return self;
    
}

- (void)sendMessage:(NSString *)message completion:(void (^)(SBSMessageProtoDTO *message, NSError *error))completion {
    
    SBSApiConversation_SendMessageRequestDTO *request = [[SBSApiConversation_SendMessageRequestDTO alloc] init];
    
    request.text = message;
    [[SbsApiClient sharedClient] conversation_SendMessage:request
                                               completion:^(SBSApiConversation_SendMessageResponseDTO *response, NSError *error) {
                                                   completion(response.message, error);
                                               }];
}
#pragma mark - Pusher/Streaming messages
- (void)subscribeToChannel:(NSString *)channelName {
    
    _channelName = channelName;
    
    if (!_connected) {
     
        // will subscribe when connected..
        return;
    }
    
    [self bindChannel];
}

- (void)unsubscribe {
    [self unbindChannel];
}

- (void)bindChannel {
    
    if (!_connected) {
        return;
    }
    
    _channel = [self.client subscribeToChannelNamed:_channelName];
    [_channel bindToEventNamed:MESSAGE_EVENT_NAME handleWithBlock:^(PTPusherEvent *channelEvent) {
        // channelEvent.data is a NSDictianary of the JSON object received
        
        if (self.delegate != nil) {
            
            SBSMessageProtoDTO *message = [[SBSMessageProtoDTO alloc] initWithDictionary:channelEvent.data];
            
            [self.delegate didReceiveMessage:message];
            
        }
    }];
    
}

- (void)unbindChannel {
    if (_channel != nil && [_channel isSubscribed]) {
        [_channel unsubscribe];
    }
    
    _channel = nil;
}

- (void)pusher:(PTPusher *)pusher connectionDidConnect:(PTPusherConnection *)connection {
    NSLog(@"Pusher connected %@", connection);
    
    _connected = YES;
    
    if (_channelName != nil) {
        [self bindChannel];
    }
}

- (void)pusher:(PTPusher *)pusher connection:(PTPusherConnection *)connection didDisconnectWithError:(NSError *)error willAttemptReconnect:(BOOL)willAttemptReconnect {
    NSLog(@"Pusher disconnected %@", error);
    
    _connected = NO;
    
    [self unbindChannel];

}

- (void)pusher:(PTPusher *)pusher connection:(PTPusherConnection *)connection failedWithError:(NSError *)error {
    NSLog(@"Pusher connect failed %@", error);

    _connected = NO;
    
    [self unbindChannel];
}

//- (BOOL)pusher:(PTPusher *)pusher connectionWillAutomaticallyReconnect:(PTPusherConnection *)connection afterDelay:(NSTimeInterval)delay {
//    
//}

- (void)pusher:(PTPusher *)pusher willAuthorizeChannel:(PTPusherChannel *)channel withRequest:(NSMutableURLRequest *)request {
    
}

//- (NSDictionary *)pusher:(PTPusher *)pusher authorizationPayloadFromResponseData:(NSDictionary *)responseData {
//}
//
- (void)pusher:(PTPusher *)pusher didSubscribeToChannel:(PTPusherChannel *)channel {
    NSLog(@"Pusher did subscribe to channel %@", channel.name);
}

- (void)pusher:(PTPusher *)pusher didUnsubscribeFromChannel:(PTPusherChannel *)channel {
    NSLog(@"Pusher did unsubscribe from channel %@", channel.name);
}

- (void)pusher:(PTPusher *)pusher didFailToSubscribeToChannel:(PTPusherChannel *)channel withError:(NSError *)error {
    NSLog(@"Pusher subscribe failed %@", error);
}

- (void)pusher:(PTPusher *)pusher didReceiveErrorEvent:(PTPusherErrorEvent *)errorEvent {
    NSLog(@"Pusher error event %@", errorEvent);
}

#pragma mark - Fetching messages

/**
 get messages.. timestamps are in seconds
 */
- (void)getMessages:(NSString *)conversationId
     startTimestamp:(double)startTimestamp
       endTimestamp:(double)endTimestamp
         completion:(void (^)(NSArray *messages, BOOL endOfMessages, NSError *error))completion {
    
    SBSApiConversation_GetRequestDTO *request = [[SBSApiConversation_GetRequestDTO alloc] init];
    
    request.conversation_id = conversationId;
    request.start_mts = [NSNumber numberWithDouble:startTimestamp * 1000];
    request.end_mts = [NSNumber numberWithDouble:endTimestamp * 1000];
    request.limit = [NSNumber numberWithInt:MESSAGE_PAGE_SIZE];
    
    [[SbsApiClient sharedClient] conversation_Get:request
                                       completion:^(SBSApiConversation_GetResponseDTO *response, NSError *error) {
                                          
                                           if (error != nil) {
                                               completion(nil, NO, error);
                                               return;
                                           }
                                           
                                           BOOL end = [response.messages count] < MESSAGE_PAGE_SIZE || [response.messages count] == 0;
                                           
                                           completion(response.messages, end, nil);
                                           
                                       }];
}

@end
