//
//  DownloadImageView.m
//  Pods
//
//  Created by Peter Hsu on 2/4/16.
//
//

#import "DownloadImageView.h"

@interface DownloadImageView()

@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UIActivityIndicatorView *indicator;

@property (nonatomic, strong) NSURLConnection *connection;
@property (nonatomic, strong) NSMutableData *data;

@end

@implementation DownloadImageView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        
        _indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        _indicator.hidesWhenStopped = YES;
        
        _indicator.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);
        [self addSubview:_indicator];
        
        [_indicator startAnimating];
        
        _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        
        [self addSubview:_imageView];

    }
    
    return self;
}

- (void)connection:(NSURLConnection *)theConnection didReceiveData:(NSData *)incrementalData {
    [_data appendData:incrementalData];
}

//the URL connection calls this once all the data has downloaded
- (void)connectionDidFinishLoading:(NSURLConnection*)theConnection {
    //so self data now has the complete image
    
    _connection = nil;
    [_indicator stopAnimating];
    
    _imageView.image = [UIImage imageWithData:_data];
    
    _data = nil;
    
    _imageView.contentMode = UIViewContentModeScaleAspectFit;
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    
    [_indicator stopAnimating];
    
}


- (void)setUrl:(NSString *)url {
    
    NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    
    self.imageView.image = nil;
    [_indicator startAnimating];
    
    if (_connection != nil) {
        [_connection cancel];
    }
    _data = [[NSMutableData alloc] init];
    
    _connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [_connection start];
    
}

- (UIImageView *)imageView {
    return _imageView;
}

@end
