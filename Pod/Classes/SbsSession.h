//
//  SbsSession.h
//  Pods
//
//  Created by Peter Hsu on 2/2/16.
//
//

#import <Foundation/Foundation.h>

#import "SbsProtoDTO.h"

@interface SbsSession : NSObject

@property (nonatomic, strong) SBSUserProtoDTO *user;
@property (nonatomic, strong) NSString *authToken;
@property (nonatomic, strong) NSString *channelName;
@property (nonatomic, strong) NSString *apiKey;

@end
