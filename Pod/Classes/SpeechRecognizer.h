//
//  SpeechRecognizer.h
//  SoySDK
//
//  Created by Peter Hsu on 4/26/16.
//  Copyright © 2016 Peter Hsu. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <SpeechKit/SpeechKit.h>

@protocol SpeechRecognizerDelegate <NSObject>
    
- (void)didSpeechRecognitionStart;
- (void)didSpeechRecognitionFinish;
- (void)didSpeechRecognitionFinishWithError:(NSError *)error;
- (void)didSpeechRecognitionReceiveTextInput:(NSString *)text;

@end


@interface SpeechRecognizer : NSObject <SKTransactionDelegate>

- (void)startSpeechRecognition;
- (void)cancelSession;

@property(nonatomic, unsafe_unretained) id<SpeechRecognizerDelegate> delegate;

@end
