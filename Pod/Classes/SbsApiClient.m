//
// SbsApiClient.m
//

#import "SbsApiClient.h"
#import "SbsNetwork.h"
#import "SbsRequest.h"
#import "SbsProtoDTO.h"


@interface SbsApiClient()
@property (nonatomic, strong) SbsNetwork *network;
@end

@implementation SbsApiClient

static SbsApiClient *sharedClient = nil;
+ (SbsApiClient *)sharedClient {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedClient = [[self alloc] init];
    });
    return sharedClient;
}

- (id)init {
    self = [super init];
    if (self) {
        _network = [[SbsNetwork alloc] initWithBaseUrl:@"https://api.soychat.com"];
    }
    return self;
}

- (void)conversation_List:(SBSApiConversation_ListRequestDTO *)request
    completion:(void (^)(SBSApiConversation_ListResponseDTO *response, NSError *error))completion {

    SbsRequest *sbsRequest = [[SbsRequest alloc] initWithEndpoint:@"conversation/list"
                                                       parameters:[request params]
                                                       httpMethod:@"POST"
                                                    responseClass:SBSApiConversation_ListResponseDTO.class];

    [_network enqueueRequest:sbsRequest
                     handler:^(BaseDTO *response, NSError *error) {
                         SBSApiConversation_ListResponseDTO *res = (SBSApiConversation_ListResponseDTO *)response;

                         completion(res, error);
                     }];

}
- (void)conversation_Get:(SBSApiConversation_GetRequestDTO *)request
    completion:(void (^)(SBSApiConversation_GetResponseDTO *response, NSError *error))completion {

    SbsRequest *sbsRequest = [[SbsRequest alloc] initWithEndpoint:@"conversation/get"
                                                       parameters:[request params]
                                                       httpMethod:@"POST"
                                                    responseClass:SBSApiConversation_GetResponseDTO.class];

    [_network enqueueRequest:sbsRequest
                     handler:^(BaseDTO *response, NSError *error) {
                         SBSApiConversation_GetResponseDTO *res = (SBSApiConversation_GetResponseDTO *)response;

                         completion(res, error);
                     }];

}
- (void)conversation_SendMessage:(SBSApiConversation_SendMessageRequestDTO *)request
    completion:(void (^)(SBSApiConversation_SendMessageResponseDTO *response, NSError *error))completion {

    SbsRequest *sbsRequest = [[SbsRequest alloc] initWithEndpoint:@"conversation/send_message"
                                                       parameters:[request params]
                                                       httpMethod:@"POST"
                                                    responseClass:SBSApiConversation_SendMessageResponseDTO.class];

    [_network enqueueRequest:sbsRequest
                     handler:^(BaseDTO *response, NSError *error) {
                         SBSApiConversation_SendMessageResponseDTO *res = (SBSApiConversation_SendMessageResponseDTO *)response;

                         completion(res, error);
                     }];

}
- (void)conversation_GetFeedbacks:(SBSApiConversation_GetFeedbacksRequestDTO *)request
    completion:(void (^)(SBSApiConversation_GetFeedbacksResponseDTO *response, NSError *error))completion {

    SbsRequest *sbsRequest = [[SbsRequest alloc] initWithEndpoint:@"conversation/get_feedbacks"
                                                       parameters:[request params]
                                                       httpMethod:@"POST"
                                                    responseClass:SBSApiConversation_GetFeedbacksResponseDTO.class];

    [_network enqueueRequest:sbsRequest
                     handler:^(BaseDTO *response, NSError *error) {
                         SBSApiConversation_GetFeedbacksResponseDTO *res = (SBSApiConversation_GetFeedbacksResponseDTO *)response;

                         completion(res, error);
                     }];

}
- (void)conversation_UpdateFeedback:(SBSApiConversation_UpdateFeedbackRequestDTO *)request
    completion:(void (^)(SBSApiConversation_UpdateFeedbackResponseDTO *response, NSError *error))completion {

    SbsRequest *sbsRequest = [[SbsRequest alloc] initWithEndpoint:@"conversation/update_feedback"
                                                       parameters:[request params]
                                                       httpMethod:@"POST"
                                                    responseClass:SBSApiConversation_UpdateFeedbackResponseDTO.class];

    [_network enqueueRequest:sbsRequest
                     handler:^(BaseDTO *response, NSError *error) {
                         SBSApiConversation_UpdateFeedbackResponseDTO *res = (SBSApiConversation_UpdateFeedbackResponseDTO *)response;

                         completion(res, error);
                     }];

}
- (void)conversation_Report:(SBSApiConversation_ReportRequestDTO *)request
    completion:(void (^)(SBSApiConversation_ReportResponseDTO *response, NSError *error))completion {

    SbsRequest *sbsRequest = [[SbsRequest alloc] initWithEndpoint:@"conversation/report"
                                                       parameters:[request params]
                                                       httpMethod:@"POST"
                                                    responseClass:SBSApiConversation_ReportResponseDTO.class];

    [_network enqueueRequest:sbsRequest
                     handler:^(BaseDTO *response, NSError *error) {
                         SBSApiConversation_ReportResponseDTO *res = (SBSApiConversation_ReportResponseDTO *)response;

                         completion(res, error);
                     }];

}
- (void)device_ListServices:(SBSApiDevice_ListServicesRequestDTO *)request
    completion:(void (^)(SBSApiDevice_ListServicesResponseDTO *response, NSError *error))completion {

    SbsRequest *sbsRequest = [[SbsRequest alloc] initWithEndpoint:@"device/list_services"
                                                       parameters:[request params]
                                                       httpMethod:@"POST"
                                                    responseClass:SBSApiDevice_ListServicesResponseDTO.class];

    [_network enqueueRequest:sbsRequest
                     handler:^(BaseDTO *response, NSError *error) {
                         SBSApiDevice_ListServicesResponseDTO *res = (SBSApiDevice_ListServicesResponseDTO *)response;

                         completion(res, error);
                     }];

}
- (void)device_DisconnectService:(SBSApiDevice_DisconnectServiceRequestDTO *)request
    completion:(void (^)(SBSApiDevice_DisconnectServiceResponseDTO *response, NSError *error))completion {

    SbsRequest *sbsRequest = [[SbsRequest alloc] initWithEndpoint:@"device/disconnect_service"
                                                       parameters:[request params]
                                                       httpMethod:@"POST"
                                                    responseClass:SBSApiDevice_DisconnectServiceResponseDTO.class];

    [_network enqueueRequest:sbsRequest
                     handler:^(BaseDTO *response, NSError *error) {
                         SBSApiDevice_DisconnectServiceResponseDTO *res = (SBSApiDevice_DisconnectServiceResponseDTO *)response;

                         completion(res, error);
                     }];

}
- (void)user_Authenticate:(SBSApiUser_AuthenticateRequestDTO *)request
    completion:(void (^)(SBSApiUser_AuthenticateResponseDTO *response, NSError *error))completion {

    SbsRequest *sbsRequest = [[SbsRequest alloc] initWithEndpoint:@"user/authenticate"
                                                       parameters:[request params]
                                                       httpMethod:@"POST"
                                                    responseClass:SBSApiUser_AuthenticateResponseDTO.class];

    [_network enqueueRequest:sbsRequest
                     handler:^(BaseDTO *response, NSError *error) {
                         SBSApiUser_AuthenticateResponseDTO *res = (SBSApiUser_AuthenticateResponseDTO *)response;

                         completion(res, error);
                     }];

}
- (void)user_Login:(SBSApiUser_LoginRequestDTO *)request
    completion:(void (^)(SBSApiUser_AuthenticateResponseDTO *response, NSError *error))completion {

    SbsRequest *sbsRequest = [[SbsRequest alloc] initWithEndpoint:@"user/login"
                                                       parameters:[request params]
                                                       httpMethod:@"POST"
                                                    responseClass:SBSApiUser_AuthenticateResponseDTO.class];

    [_network enqueueRequest:sbsRequest
                     handler:^(BaseDTO *response, NSError *error) {
                         SBSApiUser_AuthenticateResponseDTO *res = (SBSApiUser_AuthenticateResponseDTO *)response;

                         completion(res, error);
                     }];

}
- (void)user_Register:(SBSApiUser_RegisterRequestDTO *)request
    completion:(void (^)(SBSApiUser_AuthenticateResponseDTO *response, NSError *error))completion {

    SbsRequest *sbsRequest = [[SbsRequest alloc] initWithEndpoint:@"user/register"
                                                       parameters:[request params]
                                                       httpMethod:@"POST"
                                                    responseClass:SBSApiUser_AuthenticateResponseDTO.class];

    [_network enqueueRequest:sbsRequest
                     handler:^(BaseDTO *response, NSError *error) {
                         SBSApiUser_AuthenticateResponseDTO *res = (SBSApiUser_AuthenticateResponseDTO *)response;

                         completion(res, error);
                     }];

}
- (void)user_Me:(SBSApiUser_MeRequestDTO *)request
    completion:(void (^)(SBSApiUser_MeResponseDTO *response, NSError *error))completion {

    SbsRequest *sbsRequest = [[SbsRequest alloc] initWithEndpoint:@"user/me"
                                                       parameters:[request params]
                                                       httpMethod:@"POST"
                                                    responseClass:SBSApiUser_MeResponseDTO.class];

    [_network enqueueRequest:sbsRequest
                     handler:^(BaseDTO *response, NSError *error) {
                         SBSApiUser_MeResponseDTO *res = (SBSApiUser_MeResponseDTO *)response;

                         completion(res, error);
                     }];

}
@end
