//
//  SbsNetwork.h
//  Pods
//
//  Created by Peter Hsu on 1/29/16.
//
//

#import <Foundation/Foundation.h>

#import "BaseDTO.h"
#import "SbsRequest.h"

typedef void (^SbsResponseHandler)(BaseDTO *response, NSError *error);

@interface SbsNetwork : NSObject

- (id)initWithBaseUrl:(NSString *)baseURL;

- (void)enqueueRequest:(SbsRequest *)request
               handler:(SbsResponseHandler)handler;

@end
