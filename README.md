# SoySDK

[![CI Status](http://img.shields.io/travis/Peter Hsu/SoySDK.svg?style=flat)](https://travis-ci.org/Peter Hsu/SoySDK)
[![Version](https://img.shields.io/cocoapods/v/SoySDK.svg?style=flat)](http://cocoapods.org/pods/SoySDK)
[![License](https://img.shields.io/cocoapods/l/SoySDK.svg?style=flat)](http://cocoapods.org/pods/SoySDK)
[![Platform](https://img.shields.io/cocoapods/p/SoySDK.svg?style=flat)](http://cocoapods.org/pods/SoySDK)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

SoySDK is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "SoySDK"
```

## Author

Peter Hsu, silversc3@yahoo.com

## License

SoySDK is available under the MIT license. See the LICENSE file for more info.
