//
//  SoyViewController.h
//  SoySDK
//
//  Created by Peter Hsu on 01/27/2016.
//  Copyright (c) 2016 Peter Hsu. All rights reserved.
//

@import UIKit;

@interface SoyViewController : UIViewController

@property (nonatomic, strong) IBOutlet UITextField *usernameInput;
@property (nonatomic, strong) IBOutlet UITextField *passwordInput;
@property (nonatomic, strong) IBOutlet UIButton *loginButton;

@end
