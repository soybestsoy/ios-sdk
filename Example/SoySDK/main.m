//
//  main.m
//  SoySDK
//
//  Created by Peter Hsu on 01/27/2016.
//  Copyright (c) 2016 Peter Hsu. All rights reserved.
//

@import UIKit;
#import "SoyAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SoyAppDelegate class]));
    }
}
