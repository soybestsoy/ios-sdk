//
//  SoyAppDelegate.h
//  SoySDK
//
//  Created by Peter Hsu on 01/27/2016.
//  Copyright (c) 2016 Peter Hsu. All rights reserved.
//

@import UIKit;

@interface SoyAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
