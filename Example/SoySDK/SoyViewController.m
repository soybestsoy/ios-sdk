//
//  SoyViewController.m
//  SoySDK
//
//  Created by Peter Hsu on 01/27/2016.
//  Copyright (c) 2016 Peter Hsu. All rights reserved.
//

#import "SoyViewController.h"
#import <SoySDK/SoySDK.h>

@interface SoyViewController ()

@property (nonatomic, strong) UIView *loadingView;

@end

@implementation SoyViewController

@synthesize usernameInput;
@synthesize passwordInput;
@synthesize loginButton;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    loginButton.layer.cornerRadius = 4.0f;
    
    [loginButton addTarget:self
                    action:@selector(didSelectLogin:)
          forControlEvents:UIControlEventTouchUpInside];

}

- (void)didSelectLogin:(id)sender {
    
    [self showLoadingScreen:@"Authenticating"];
    [SoySDK setUser:usernameInput.text secret:passwordInput.text];
    

}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    
    // subscribe to notification status
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onSoySessionStatusChanged:)
                                                 name:SoySessionStatusNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)onSoySessionStatusChanged:(NSNotification *)notification {
    SoySessionStatus *status = notification.object;
    
    NSLog(@"Soy session status %@", status);
    
    [self hideLoadingScreen];

    if (status.status == SessionStatusAuthenticated) {
        [self showSoyView:nil];
    } else {
        [self showAlert:@"Error" message:@"Error authenticating"];
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)showSoyView:(id)sender {
    
    SoyChatViewController *soyChatController = [[SoyChatViewController alloc] init];
    
    [self presentViewController:soyChatController
                       animated:YES
                     completion:^{
                         
                         NSLog(@"Showing soychat");
                         
                     }];
    
}

- (void)showAlert:(NSString *)title message:(NSString *)message {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"OK"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * _Nonnull action) {
                                                             
                                                         }];
    
    [alert addAction:cancelAction];
    
    [alert setModalPresentationStyle:UIModalPresentationOverFullScreen];
    
    UIPopoverPresentationController *popPresenter = [alert
                                                     popoverPresentationController];
    popPresenter.sourceView = self.view;
    popPresenter.sourceRect = CGRectMake(self.view.center.x, self.view.center.y, 1, 1);
    
    [self presentViewController:alert animated:YES completion:nil];
    
}



- (void)showLoadingScreen:(NSString *)text {
    if (_loadingView == nil) {
        UIView *view = [[UIView alloc] initWithFrame:self.view.bounds];
        
        // catch touches while shown
        view.userInteractionEnabled = YES;
        
        view.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5f];
        
        UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        
        view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [view addSubview:indicator];
        
        indicator.center = CGPointMake(view.frame.size.width/2, view.frame.size.height/2);
        indicator.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin;
        [indicator startAnimating];
        _loadingView = view;
        
        UITextView *textView = [[UITextView alloc] initWithFrame:CGRectMake(0, view.frame.size.height/2 + 40, view.frame.size.width, 50)];
        textView.tag = 1000;
        textView.font = [UIFont systemFontOfSize:16];
        textView.textAlignment = NSTextAlignmentCenter;
        textView.backgroundColor = [UIColor clearColor];
        textView.textColor = [UIColor whiteColor];
        textView.editable = NO;
        textView.selectable = NO;
        textView.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin;
        [view addSubview:textView];
        
    }
    
    UITextView *textView = (UITextView *) [_loadingView viewWithTag:1000];
    if (text == nil) {
        textView.alpha = 0;
    } else {
        textView.text = text;
        textView.alpha = 1;
    }
    
    [self.view addSubview:_loadingView];
    
}
- (void)hideLoadingScreen {
    
    [_loadingView removeFromSuperview];
    _loadingView = nil;
    
}


@end
